\documentclass[11pt, a4paper]{article}

\usepackage[]{../TemplateLatex/am} % [pl] or []

%%% Definitions %%%
\university{Poznań University of Technology}
\institute{Institute of Robotics and Machine Intelligence}
\department{Division of Control and Industrial Electronics}
\lab{Mobile and embedded applications for Internet of Things}
\author{Dominik Łuczak, Ph.D.; Adrian Wójcik, M.Sc.}
\email{Dominik.Luczak@put.poznan.pl \\ Adrian.Wojcik@put.poznan.pl}

\maintitle{ Server IoT \\ Sense Hat}

\comment{Teaching materials for laboratory}
%%
%%

\begin{document}

%% First page %%
\mainpage

%%
%%

\section{Goal}

\subsection*{Knowledge}
The aim of the course is to familiarize yourself with: 
\begin{itemize}[label={\textbullet}]
\item available programming tools and libraries for the Sense Hat board,
\item structure and components of Sense Hat board,
\end{itemize}

\subsection*{Skills}
The aim of the course is to acquire skills in:
\begin{itemize}[label={\textbullet}]
\item operating sensors with digital interfaces using SBC (RPi),
\item operating output devices (LED matrix) using SBC (RPi),
\item operating input devices (joystick) using SBC (RPi),
\item developing an IoT server application.
\end{itemize}

\subsection*{Social competences}
The aim of the course is to develop proper attitudes:
\begin{itemize}[label={\textbullet}]
\item strengthening the understanding of the role and application of network communication in IT systems and related security issues,
\item strengthening understanding and awareness of the importance of non-technical aspects and effects of the engineer's activities, and the related responsibility for the decisions taken,
\item choosing the right technology and programming tools for the given problem,
\item testing developed IT system.
\end{itemize}

\section{Final Task}
% fixed component
\input{../TemplateLatex/POLECENIA_KONCOWE_ENG.tex}

\section{Prepare to course}

\subsection{Know the safety rules}
% fixed component
\input{../TemplateLatex/BHP_ENG.tex}

\subsection{Introduction to Sense Hat}
%% -- BEGIN Sense Hat introduction ----------------------------------------------------------------
Sense Hat is an add-on board for Raspberry Pi, created specifically for the Astro Pi  competition \cite{astropi}. The board allows to measure temperature, humidity, pressure and orientation, and display information using the built-in RGB LED matrix \cite{sensehatproj}. On the Raspberry Pi website, there is hardware documentation along with an electrical diagram and information about available programming tools and libraries \cite{sensehatdoc}.
\\
The Sense Hat board is equipped with:
\begin{itemize}[label={\textbullet}]
\item \textbf{LSM9DS1} - integrated circuit containing 3D digital linear acceleration sensor, a 3D digital angular rate sensor, and a 3D digital magnetic sensor. The device measures linear accelerations in the range up to $\pm$16 g, magnetic fields up to $\pm$ 16 Gs and angular velocity up to $\pm$2000 dps. This sensor contains the I2C serial bus interface supporting standard and fast mode (100 kHz and 400 kHz) and the SPI interface \cite{lsm9ds1}.
\item \textbf{HTS221} - compact relative humidity and temperature sensor. The measuring element consists of a polymer dielectric flat capacitor structure capable of detecting changes in relative humidity. It includes a built-in 16-bit analog-to-digital converter and I2C and SPI interfaces \cite{hts221}.
\item \textbf{LPS25H} - digital barometer measuring pressure in the range of 260-1260 mbar with absolute accuracy of $\pm$ 0.2 mbar and a typical noise RMS of 0.01 mbar in high resolution mode. These pressures can be easily converted to altitude. The sensor is equipped with I2C and SPI interfaces \cite{lps25h}.
\item \textbf{ATTINY88-MUR} - 8-bit microcontroller from the AVR ATtiny family, equipped with I2C and SPI interfaces, operating at a maximum speed of 12 MHz. It is an intermediary element in controlling the LED matrix output and reading the joystick input \cite{attiny88mur}.
\item \textbf{CAT24C32} - 32Kb CMOS EEPROM internally organized as 4096 words of 8 bits each. It has a 32-byte page write buffer and supports the I2C interface (100 kHz / 400 kHz / 1 MHz). External address pins allow addressing of up to eight CAT24C32 devices on the same bus. This memory contains board configurations and is an essential element of extensions boards for Raspberry Pi \cite{cat24c32}.
\item \textbf{LED2472G} - LED RGB matrix driver. The device is configured in 3 groups (red, green and blue) with 8 independently controlled channels. The LED current can be adjusted separately for each color in the range from 4 mA to 72 mA. This range is divided into two sub-ranges, and the current can be adjusted in each range in 64 resolution steps (6 bits per color) \cite{led2472g}.
\item \textbf{SKRHABE010} - 4-directional joystick with center monostable push-button.
\end{itemize}
The entire board, due to the use of the I2C bus, involves only 6 GPIO pins of the RPi connector.

%% -- END Sense Hat introduction ------------------------------------------------------------------

\subsection{Sense Hat API}
%% -- BEGIN Sense Hat API -------------------------------------------------------------------------
On the \href{https://pythonhosted.org/sense-hat/}{pythonhosted.org}  portal, is available documentation on the application programming interface (API) for the Sense Hat board implemented in Python with simple examples for every component \cite{sensehatapi}. \\

Another popular programming tool for Sense Hat is RTIMULib (real-time IMU library) \cite{rtimulib}. It is a C++ and Python library that facilitates the use of an inertial measurement unit (IMU) with 9 or 10 degrees of freedom, dedicated to embedded Linux systems (especially Raspberry PI and Intel Edison).
%% -- END Sense Hat API ---------------------------------------------------------------------------

\newpage

\subsection{Sense Hat Emulator}
%% -- BEGIN Sense Hat emulation -------------------------------------------------------------------
For the desktop versions of the Raspbian system, the Sense Hat \cite {sensehatemu} emulator is available. In fig \ ref {fig: emu}. there is a screenshot from a shared virtual machine running the Sense Hat emulator. An API in Python is available for the emulator. Using the script to communicate with a physical device requires only changing the attached library:
\begin{itemize}[label={\textbullet}]
\item Emulator: \lstinline{from sense_emu import SenseHat}
\item Physical device: \lstinline{from sense_hat import SenseHat}
\end{itemize}

\begin{figure}[H]
\includecgraphics[width=15.0cm]{emu.png}
\caption{ Sense Hat emulator available on the virtual machine. }
\label{fig:emu}
\end{figure}

\begin{tcolorbox}[colback=purple,coltext=black]
\textbf{NOTE!} To change the system boot mode from CLI to Desktop, run the command \lstinline{sudo raspi-config} and then select the appropriate configuration in the \textbf{Boot Options} menu.
\end{tcolorbox}

An online version of the emulator is also available, allowing to test programs directly in the browser \cite{trinket}.


%% -- END Sense Hat emulation ---------------------------------------------------------------------

\subsection{JSON in Python}
%% -- BEGIN JSON in Python -------------------------------------------------------------------
In Python there is available \lstinline{json} API to encode and decode data - including custom, complex objects - using JSON notation \cite{pyjsonapi}. In the listing \ref{lst:jsonpy}. there is an example script for encoding the built-in data type \textit{dictionary} (with syntax similar to JSON notation) and a simple class \lstinline{EnvData} created by the user. Fig. \ref{fig:jsonpyresult}. shows the result of successful script execution.

\newpage

\lstinputlisting[style=MyPythonStyle, 
caption={ An example of encoding data in JSON notation with Python. }, 
label={lst:jsonpy}]{lst/json_dump_test.py}

\begin{figure}[H]
\includecgraphics[width=11.5cm]{json_dump_test_result.png}
\caption{ Result of script from listing \ref{lst:jsonpy}. execution. }
\label{fig:jsonpyresult}
\end{figure}

%% -- END JSON in Python -------------------------------------------------------------------

\section{Scenario for the class}

\subsection{Teaching resources}
\begin{tabular}{ l  p{13cm} }
 Hardware   & \tabitem computer, \\
            & \tabitem Raspbery Pi single-board computer set, \\
			& \tabitem Sense-Hat add-on board, \\
            & \tabitem miroSD card, \\
            & \tabitem microUSB 5V power supply, \\
            & \tabitem RJ45 cable, \\
            & \tabitem \textit{WiFi router.} \\
 Software   & \tabitem SSH client (Bitwise SSH Client),    \\
 			& \tabitem application for writing binary disk images to external media (eg Win32DiskMaker), \\
 			& \tabitem text editor (Notepad++), \\
 			& \tabitem VirtualBox appliaction and virtual machine with Raspbian system.
\end{tabular}

\subsection{Tasks} \label{exercises}
\begin{enumerate}
%% --- exercise 01 -----------------------------------------------
\item Write a CLI application that allows you to read pressure, temperature and humidity sensors.
	\begin{enumerate}
	\item The application should take input arguments: -p -t -h (all optional), each of them should allow selection measurement unit: hPa/mmHg, C/F, \%/[number between 0-1].
	\item The presence of a given argument means that a given quantity should be measured and scaled to a given unit.
	\item The application should display the result in JSON format.
	\end{enumerate}

%% --- exercise 02 -----------------------------------------------
\item Write a CLI application that allows reading measurement from IMU (roll, pitch, yaw angles).
	\begin{enumerate}
	\item The application should take input arguments: -r, -p, -y (optional) and -u (unit: radians or degrees),
	\item The presence of a given argument means that appropriate angles (roll, pitch and yaw) should be measured and scaled to a given unit.
	\item The application should display the result in JSON format.
	\end{enumerate}
	
%% --- exercise 03 -----------------------------------------------
\item Write the CLI application allowing you to control the LED matrix. The application should allow turning on the selected LED [x: (0-7), y: (0-7)] with the selected color (r, g, b) with input arguments.

%% --- exercise 04 -----------------------------------------------
\item Write a CLI application that allows you to read the state of the joystick.
	\begin{enumerate}
	\item The application should contain a main loop, which can be left by pressing the appropriate (or any) key.
	\item The application should contain three software counters: counting: number of clicks on the center push-button, position in the x axis (right: increment, left: decrement) and position in the y axis (up: increment, down: decrement).
	\item  Counters values should be written with a constant time step into a text file (e.g. joystick.dat). Apply JSON format.
	\item While the program is running, launch the 2nd terminal and check the contents of the file while interacting with the joystick.
	\end{enumerate}	

%% --- exercise 05 -----------------------------------------------
\item Write a PHP script that allows you to view the measurements and content of the joystick.dat file.
	\begin{enumerate}
	\item Create a PHP script that will call programs from tasks 1, 2 and 4.
	\item Display the result in the browser in the form of JSON format (i.e. aggregate objects).
	\end{enumerate}

%% --- exercise 06 -----------------------------------------------
\item Write a PHP script that allows you to set the color of the selected LEDs:
	\begin{enumerate}
	\item Create an HTML form with 8x8 text inputs - see: \textit{PHP instruction}.
	\item Use the POST method to send information to a PHP script.
	\item Control the appropriate LEDs based on the data from the form with program from task 3.
	\end{enumerate}

%% --- exercise 07 -----------------------------------------------
\item \textbf{(*)} Additional tasks (requires a physical device). Modify the demo in: \\
\lstinline {/home/pi/sense-hat/RTIMULib/RTIMULibDrive11} so that:
	\begin{enumerate}
	\item Information about the initialization result was redirected to the ,,rtimulibdrive11.log'' file
	\item The next measurement results should be saved (overwritten) to the ,,rtimulibdrive11.dat'' file in JSON format.
	\item Write a PHP script that allows you to display ,,rtimulibdrive11.dat''.
	\item Add input arguments to the program allowing you to choose which measurements are performed and saved to a file.
	\end{enumerate}

\end{enumerate}

\bibliographystyle{IEEEtran}
\bibliography{../TemplateLatex/AM_REF} 

\end{document}
