import json

# synthetic data 
temp_val = "20.5C"
press_val = "1023.0hPa"

# use Python Dictionaries
print("Python Dictionaries:")
press_name = "press"
temp_name = "temp"
dict_data = {temp_name : temp_val, press_name : press_val}
result = json.dumps(dict_data)
print(dict_data)
print(result)

# use custom class
print("\nCustom class:")
class EnvData:
	def __init__(self, temp, press):
		self.temp = temp
		self.press = press

obj_data = EnvData(temp_val, press_val)
result = json.dumps(obj_data.__dict__)
print(obj_data)
print(result)