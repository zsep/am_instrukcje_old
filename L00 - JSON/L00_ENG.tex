\documentclass[11pt, a4paper]{article}

\usepackage[]{../TemplateLatex/am} % [pl] or []

%%% Definitions %%%
\university{Poznań University of Technology}
\institute{Institute of Robotics and Machine Intelligence}
\department{Division of Control and Industrial Electronics}
\lab{Mobile and embedded applications for Internet of Things}
\author{Dominik Łuczak, Ph.D.; Adrian Wójcik, M.Sc.}
\email{Dominik.Luczak@put.poznan.pl \\ Adrian.Wojcik@put.poznan.pl}

\maintitle{Data-interchange format JSON}

\comment{Teaching materials for laboratory}
%%
%%

\begin{document}

%% First page %%
\mainpage

%%
%%

\section{Goal}

\subsection*{Knowledge}
The aim of the course is to familiarize yourself with: 
\begin{itemize}[label={\textbullet}]
\item JSON format syntax, 
\item data formats used in IT systems,
\item available methods of parsing data in JSON format.
\end{itemize}

\subsection*{Skills}
The aim of the course is to acquire skills in:
\begin{itemize}[label={\textbullet}]
\item interpretation of data in JSON format, 
\item using the JSON format in mobile applications,
\item simple data representation as structure in JSON format,
\item complex data representation as structure in JSON format,
\end{itemize}

\subsection*{Social competences}
The aim of the course is to develop proper attitudes:
\begin{itemize}[label={\textbullet}]
\item strengthening the understanding and awareness of the importance of non-technical aspects and effects of the engineer's activities and the related responsibility for the decisions taken,
\item selection of appropriate data structures for a given problem.
\end{itemize}

\section{Final Task}
% fixed component
\input{../TemplateLatex/POLECENIA_KONCOWE_ENG.tex}

\section{Prepare to course}

\subsection{Know the safety rules}
% fixed component
\input{../TemplateLatex/BHP_ENG.tex}

\subsection{Introduction to the JSON format}
JSON (JavaScript Object Notation) is an open standard for the format of computer data exchange in the form of human-readable text \cite{l01}. Objects in JSON format consist of attribute - value pairs. Listing \ref{lst:lst1}. presents a simple object consisting of a single primitive.

\begin{lstlisting}[style=MyJavaScriptStyle, caption={ Basics of JSON syntax - an object in the form of an attribute (key) / value pair. }, label={lst:lst1}]
			{ "attribute" : "value" }                        
\end{lstlisting}

Formally, JSON is a subset of the JavaScript language, but in practice, it is a format independent of the programming language. One of the basic advantages of the format is the availability of libraries enabling its use, among others in C, C++, C\#, Java, PHP, Python and many more.
The basic application of the JSON format is data transfer in network applications based on the client-server model, including applications based on the REST (Representational State Transfer architecture). The technology is used by websites such as Google, YouTube, Twitter, Facebook and many more \cite{l02}.
 
\subsection{JSON syntax}
The message in JSON format is an associative (associative) table. All data is variable, which means that the format does not allow the transfer of executable code. Attribute names (fields, components) are always surrounded by quotation marks. Subsequent attribute-value pairs are separated by a comma. There is no comma after the last element. Whitespace is ignored in the parsing process. The message is coded in Unicode and by default, it is UTF-8 \cite {l03}.
There are four types of attribute values:
\begin{itemize}[label = {\textbullet}]
\item string (inscription surrounded by quotation marks),
\item number (a floating-point number),
\item boolean value (true or false),
\item null value.
\end{itemize}
 
Listing \ref{lst:lst2}. shows an example of an object in JSON format containing the address of Poznan University of Technology.

\begin{lstlisting}[style=MyJavaScriptStyle, caption={ Poznan University of Technology address in JSON format. }, label={lst:lst2}]
{  
 "street name" : "M. Sklodowska-Curie Square",  
 "building number" : 5, 
 "apartment number" : null, 
 "post town" : "Poznan", 
 "postcode" : "60-965" 
}
\end{lstlisting}

The JSON format also allows data representation using arrays. The tables are an ordered list of data (not necessarily the same type!) Enclosed in square brackets and separated by a comma. Listing \ref{lst:lst3}. shows an example of a simple array containing the names of the faculties of Poznan University of Technology.  

\begin{lstlisting}[style=MyJavaScriptStyle, caption={ An array containing the names of the faculties of Poznan University of Technology in JSON format. }, label={lst:lst3}]
[   "Architecture",   
    "Control, Robotics and Electrical Engineering",  
    "Computing and Telecommunications",  
    "Civil and Transport Engineering",   
    "Materials Engineering and Technical Physics",   
    "Mechanical Engineering",   
    "Environmental Engineering and Energy",   
    "Engineering Management",   
    "Chemical Technology"    ] 
\end{lstlisting}

The basic property of the JSON format is the ability to nest objects freely. Listing \ref{lst:lst1}. shows an example of nesting objects. Nesting arrays can be done in a similar way. 
\newpage
\begin{lstlisting}[style=MyJavaScriptStyle, caption={ Nesting of objects in JSON format based on contact details of three selected faculties of Poznan University of Technology.}, label={lst:lst4}]
{ 
 "Faculty of Control, Robotics and Electrical Engineering" : { 
  "postal address" : "Piotrowo 3A, 60-965 Poznan", 
  "phone number" : "+48 61 665 25 39" 
 }, 
  
 "Faculty of Computing and Telecommunications" : { 
  "postal address" : "Piotrowo 3A, 60-965 Poznan", 
  "phone number" : "+48 61 665 34 20" 
 }, 
  
 "Engineering Management" : { 
  "postal address" : "Piotrowo 3, 60-965 Poznan", 
  "phone number" : "+48  61 665 2360" 
 } 
}
\end{lstlisting}

The JSON format provides great freedom in how data is represented. The programmer's task is to properly analyze the problem and propose a form of data representation appropriate for the task. 

\section{Scenario for the class}

\subsection{Teaching resources}
\begin{tabular}{ l  p{13cm} }
 Hardware  & \tabitem computer, \\
           & \\
 Software  & \tabitem source code editor (Notepad++).  \\
 		   & \tabitem JSON parser (web application).	
\end{tabular}

\subsection{Tasks} \label{exercises}
\begin{enumerate}
\item Familiarize yourself with the JSON format syntax. Make a syntactic analysis of the presented examples using the parser indicated by the instructor (e.g. \url {http://json.parser.online.fr/} or \url {http://jsonviewer.stack.hu/}).

\item A simple data structure in JSON format.
\begin{enumerate}
\item An object that stores information about colors.
\begin{enumerate}
\item Create a simple object containing information about the color in the RGB model (e.g. R = 0, G = 100, B = 200).
\item The object should consist of three numeric values: R, G, and B. 
\item Verify object syntax using a parser.
\end{enumerate}

\item Color array.
\begin{enumerate}
\item Create a simple array containing information about the color in the RGB model (e.g. R = 0, G = 100, B = 200).
\item The array should consist of three numeric values. 
\item Note how the two representations differ? Which character contains more information? Verify object syntax using a parser.
\end{enumerate}
 
\item An object that stores information about colors in the form of an array.
\begin{enumerate}
\item Create a simple object containing information about the color in the RGB model (e.g. R = 0, G = 100, B = 200).
\item The object should consist of one attribute-value pair RGB with an array value.
\item Note how the two representations differ? Which character contains more information? Verify object syntax using a parser.
\end{enumerate}
\end{enumerate}

\item A complex data structure in JSON format.

\begin{enumerate}
\item Create a complex object consisting of RGB objects representing the set of colors (e.g. RGB1 = 0.0.0; RGB2 = 0.100.200; RGB3 = 255.255.255; RGB4 = 255.0.0). Each subobject should contain three numeric values: R, G, and B. Verify object syntax using a parser.
\item Create an array consisting of RGB arrays representing the set of colors (e.g. 0.0.0; 0.100.200; 255.255.255; 255.0.0). Verify object syntax using a parser.
\item Create an object consisting of elements containing an RGB color arrays, representing the set of colors (e.g. RGB1 = 0.0.0; RGB2 = 0.100.200; RGB3 = 255.255.255; RGB4 = 255.0.0). Verify object syntax using a parser.
\end{enumerate}

\item A complex problem of data representation.

\begin{enumerate}
\item Suggest a structure for representing your timetable in the current semester.
\item Note the data redundancy in the proposed representation. Consider the object-oriented application of data.
\item Create a data structure in JSON format to represent the required data.
\item Verify object syntax using a parser.
\end{enumerate}

\item Data representation structure - embedded system.

\begin {enumerate}
\item Suggest a data format representing the state of digital I / Os. Use at least 4 inputs and 4 outputs.
\item Suggest a data format that represents the status of the analog inputs/outputs. Use at least 4 inputs and 4 outputs.
\item Extend the format of the digital I / O data with additional information indicated by the lecturer (e.g. device name, unique channel number, channel name, type of sensor/actuator, measurement frequency, date of last reading, activation level: low, high, slope).
\item Extend the format of the analog input/output data with additional information indicated by the lecturer (e.g. device name, unique channel number, channel name, type of sensor/actuator, resolution, accuracy, refresh rate, date of last input/output update, unit of the measured/set physical quantity, range: minimum and maximum value, type of input signal filtering).
\item Suggest a structure describing the measuring sensor indicated by the teacher (e.g. analog temperature sensor)
\end{enumerate}

\item (\textbf{*}) Data representation structure for the Sense-HAT extension board. Based on the available device documentation (\url {https://www.raspberrypi.org/documentation/hardware/sense-hat/}) propose a transmitting and receiving data structure in JSON format

\end{enumerate}

\bibliographystyle{IEEEtran}
\bibliography{../TemplateLatex/AM_REF} 

\end{document}