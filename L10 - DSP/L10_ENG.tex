\documentclass[11pt, a4paper]{article}

\usepackage[]{../TemplateLatex/am} % [pl] or []

%%% Definitions %%%
\university{Poznań University of Technology}
\institute{Institute of Robotics and Machine Intelligence}
\department{Division of Control and Industrial Electronics}
\lab{Mobile and embedded applications for Internet of Things}
\author{Dominik Łuczak, Ph.D.; Adrian Wójcik, M.Sc.}
\email{Dominik.Luczak@put.poznan.pl \\ Adrian.Wojcik@put.poznan.pl}


\maintitle{Digital Signal Processing}

\comment{Teaching materials for laboratory}
%%
%%

\begin{document}

%% First page %%
\mainpage

%%
%%

\section{Goal}

\subsection*{Knowledge}
The aim of the course is to familiarize yourself with: 
\begin{itemize}[label={\textbullet}]
\item an example of the DSP use in IoT applications,
\item using dedicated tools for DSP in the application development process,
\item test-driven software development technique,
\end{itemize}

\subsection*{Skills}
The aim of the course is to acquire skills in:
\begin{itemize}[label={\textbullet}]
\item creating automatic scripts that generate and analyze DSP algorithms,
\item implementing simple DSP algorithms in general-purpose languages,
\item proper testing of DSP algorithms in target environments,
\item integration of DSP algorithms with IoT applications.
\end{itemize}

\subsection*{Social competences}
The aim of the course is to develop proper attitudes: 
\begin{itemize}[label={\textbullet}]
\item using systematic software development techniques and methodologies,
\item strengthening understanding and awareness of the importance of non-technical aspects and effects of the engineer's activities, and the related responsibility for the decisions taken,
\item choosing the right technology and programming tools for the given problem, 
\item testing developed IT system.
\end{itemize}

\section{Final Task}
% fixed component
\input{../TemplateLatex/POLECENIA_KONCOWE_ENG.tex}

\section{Prepare to course}

\subsection{Know the safety rules}
% fixed component
\input{../TemplateLatex/BHP_ENG.tex}

\subsection{Example DSP algorithm: low pass filter}
%% -- BEGIN LP FIR ----------------------------------------------------------------
\textit {Digital Signal Processing} (DSP) is a key element of modern control and measurement systems. Therefore, it is also a necessary element of many applications for Internet of Things (IoT). The correct implementation of DSP algorithms - which in practice means \textit{sufficiently tested} - is therefore an important element in the development of IoT systems. However, the implementation of DSP algorithms is a complex, interdisciplinary task involving signals and dynamic systems theory, numerical analysis and software development. Consequently, it requires knowledge of theoretical basis of the issue and use of adequate programming tools. \\

One of the DSP algorithms used in a very wide range of applications are \textit{digital filters}. The instruction presents an example of the implementation of a low-pass filter with a finite impulse response (FIR filter). In fig. \ref{fig:workflow} a diagram of the adopted software development technique is presented. The presented procedure consists of the following stages:
\begin{enumerate}
\item Filter design using the GNU Octave \cite{octave} environment and (alternatively) the SciPy \cite{scipy} library.
\item The implementation of the filter for client applications of the IoT system in three general-purpose languages: Java, JavaScript and C\#, for three different environments, respectively: a mobile application for Android, a web application and a desktop application for Windows.
\item Conducting automatic unit tests of implemented algorithms.
\item Integration of algorithms with demo applications with network communication and a graphical user interface (GUI), i.e. emulating real use cases of DSP.
\end{enumerate}

\begin{figure}[H]
\includecgraphics[width=10.0cm]{workflow.png}
\caption{ Schematic diagram of the adopted development technique for implementing DSP algorithm. }
\label{fig:workflow}
\end{figure}

\subsubsection{Filter design procedure}
%% -- BEGIN LP FIR: design --------------------------------------------------------
Most software tools for digital signal processing and analysis include graphical digital filter design tools. However, in software development, automation and repeatability of processes - including design processes - is often very important. Hence the presented example is based on \textit{scripted} procedure for designing FIR filter. Available libraries for DSP, such as the \textbf{signal} package for the GNU Octave environment or the \textbf{SciPy} library written in Python, contain procedures to obtain FIR filter coefficients based on minimum necessary information: filter order, cutoff frequencies and filter type (low-, high- or bandpass). The FIR filter can be expressed as (\ref{eq0}). The filter design involves finding a set of $b_k$ coefficients that will allow to obtain the assumed frequency or time characteristics.

\begin{equation}\label{eq0}
x_f(k)=b_0x(k)+b_1x(k-1)+b_2x(k-2)+\cdots+b_Nx(k-N)
\end{equation}
\vspace{0.1cm}

\begin{tabular}{ r c c  p{13cm} }
 where:  & $x_f$ & - & filter output - a filtered digital signal, \\
 		 & $x$ & - & filter input - an original digital signal, \\
         & $k$ & - & sample number, \\
         & $N$ & - & filter order, \\
         & $b_{0\cdots N}$ & - & filter feedforward coefficients, \\
\end{tabular}
\vspace{0.5cm}

In the example, assumptions were made regarding the frequency characteristics of the filter: cutoff frequencies and attenuation level. \textit{harris} method, presented in the equation (\ref{eq1}), was used to estimate the low-pass filter order based on the assumed transition bandwidth and attenuation in the stopband  \cite{harris}.

\begin{equation}\label{eq1}
\hat{N}=\frac{f_s}{f_2-f_1}\cdot\frac{A_{dB}}{22}
\end{equation}
\vspace{0.1cm}

\begin{tabular}{ r c c  p{13cm} }
 where:  & $\hat{N}$ & - & approximated filter order, \\
 		 & $f_s$ & - & sampling frequency, \\
         & $f_1$ & - & passband cutoff frequency, \\
         & $f_2$ & - & stopband cutoff frequency, \\
         & $A_{dB}$ & - & assumed attenuation in stopband, \\
\end{tabular}
\vspace{0.5cm}

The example assumes the following design parameters for the filter:
\begin{itemize} [label={\textbullet}]
\item Sampling period $t_s$ = 100 ms; i.e. sampling frequency $f_s$ = 10 Hz - this is a frequency that can easily be achieved using home wireless networks (e.g. WiFi).
\item Passband cutoff frequency $f_1$ = 0.5 Hz - so we assume that frequencies higher than 0.5 Hz are caused by noise e.g. measurement errors.
\item Stopband cutoff frequency $f_2$ = 0.8 Hz - at this frequency the filter should have the assumed attenuation of $ A_{dB}$.
\item Assumed attenuation in stopband $ A_{dB} $ = 60 dB - attenuation corresponding to a 1000-fold decrease in signal amplitude.
\item With the adopted assumptions and filter order estimation method (\ref{eq1}), the filter order  $N$ will be equal to 91, which corresponds to 92 filter coefficients.
\end{itemize}

The filter design and analysis procedure was carried out as follows:
\begin{enumerate}
\item A test signal vector was generated consisting of four harmonic components with unit amplitude and frequencies $f$ = \{0.1, 0.2, 0.7, 1.0 \}. The amplitude of the first two components should not change after applying filter, while the amplitude of the last two should be significantly reduced, i.e. the last two components will be \textit{filtered}. Signal amplitude spectrum was computed.
\item Assumed filter parameters have been defined.
\item Filter order was estimated using(\ref{eq1}).
\item Filter coefficients were obtained using the available library functions: \lstinline{fir1} in GNU Octave \cite{octavefir1} and \lstinline{firwin} in SciPy \cite{scipyfirwin}. Both of these methods by deafault use the Hamming window \cite{window}.
\item The frequency characteristics of the resulting filter were generated by performing Fourier transform on filter coefficients - for the FIR filter it is an equivalent to transform of filter impulse response.
\item The test signal was filtered using the available library functions: \lstinline{filter} in GNU Octave \cite{octavefilter} and \lstinline{lfilter} in SciPy \cite{scipylfilter}. Signal amplitude spectrum after filtration was computed.
\end{enumerate}

%% -- END LP FIR: design ----------------------------------------------------------

\subsubsection{Filter design in GNU Octave environment}
%% -- BEGIN LP FIR: octave --------------------------------------------------------
In listing \ref{lst:octave}. presents GNU Octave environment script implementing procedure of designing, analyzing and testing digital filter. In fig. \ref{fig:octave}. results of this procedure are presented in a visual form: signal time series before and after filtration and the signal amplitude spectrum before and after filtration in combination with frequency characteristics of designed filter.

\lstinputlisting[style=MyMATLABStyle, 
caption={ Filter design and analysis - Octave environment. },
label={lst:octave}]{lst/octave.m}

\vspace{0.1cm}

\begin{figure}[H]
\includecgraphics[width=16.0cm]{octave.png}
\caption{ Visualization of filter analysis - Octave environment. }
\label{fig:octave}
\end{figure}

Based on the obtained results, were generated source files (\lstinline{.java}, \lstinline{.js}, \lstinline {.cs}) containing parameters (vector \lstinline{H}) and initial filter state (vector with \lstinline{N+1} zeros) as well as unit test data in the form of 500 samples of both original test signal and signal after filtration.

%% -- END LP FIR: octave ----------------------------------------------------------

\subsubsection{Filter design in Python with SciPy}
%% -- BEGIN LP FIR: python --------------------------------------------------------
In listing \ref{lst:scipy}. is presented a script written in Python using the SciPy library that implements the procedure of designing, analyzing and testing a digital filter. In fig. \ref{fig:octave}. results of this procedure are presented in a visual form: signal time series before and after filtration and the signal amplitude spectrum before and after filtration in combination with frequency characteristics of designed filter. Comparing listings \ref{lst:octave}. and \ref{lst:scipy}. it is not difficult to notice similarity of all stages of both procedure as well as similarity of library functions interfaces. As expected, the results of both procedures coincide.
\vspace{0.5cm}
\lstinputlisting[style=MyPythonStyle, 
caption={ Filter design and analysis - Python with SciPy library. },
label={lst:scipy}]{lst/scipy.py}

\begin{figure}[H]
\includecgraphics[width=17.0cm]{scipy.png}
\caption{ Visualization of filter analysis - Python with SciPy library.  }
\label{fig:scipy}
\end{figure}
%% -- END LP FIR: python ----------------------------------------------------------

%% -- END LP FIR ------------------------------------------------------------------

\subsection{Implementation of DSP in general-purpose programming languages}
After carrying out presented filter design procedure and obtaining source files for  mobile app (Java), web app (JavaScript) and desktop app (C\#) the implementation of the FIR filter algorithm was carried out in all above-mentioned general-purpose languages. The filtration task is a frequently used software procedure, so for each of the languages listed you can use available (often free) libraries containing ready implementations. In the example, however, attention is drawn to the fact that the language in which DSP algorithm is implemented is a secondary issue to the algorithm itself. Unless dedicated equipment is used, the algorithm stages will usually be implemented in a similar way regardless of the language used, assuming the same paradigm is used. In all the examples of implementation of FIR filter object-oriented technique is used: filter is an object containing information about the coefficients and filter state, the initialization procedure in the form of a class constructor and the \lstinline{Execute} method performing a single algorithm step. \\

It is worth noting that in a typical use of the \textit{test driven development}, i.e. test-based software development technique, test creation should be done ahead of the actual implementation of functionality \cite{ut}.

\subsubsection{FIR filter in Java}

In listing \ref{lst:firjava}. an example implementation of FIR filter in Java is presented, based on the equation (\ref {eq0}), in the form of \lstinline{MyFir} class. In order to easily modify the filter state, the \lstinline {ArrayList} class was used as the container for this data.
\vspace{0.5cm}

\lstinputlisting[style=MyJavaStyle, 
caption={ FIR filter implementation in Java.  },
label={lst:firjava}]{lst/fir.java}

\subsubsection{FIR filter in JavaScript}

In listing \ref{lst:firjs}. an example implementation of FIR filter in JavaScript is presented, based on the equation (\ref{eq0}), in the form of \lstinline{MyFir} class.

\lstinputlisting[style=MyJavaScriptStyle, 
caption={ FIR filter implementation in JavaScript. },
label={lst:firjs}]{lst/fir.js}

\subsubsection{FIR filter in C\#}

In listing \ref{lst:fircs}. an example implementation of the FIR filter in C\# is presented, based on the equation (\ref{eq0}), in the form of the \lstinline{MyFir} class. In order to easily modify the filter state, the \lstinline{List} class was used as the container for this data.
\vspace{0.5cm}

\lstinputlisting[style=MyCSStyle, 
caption={ FIR filter implementation in C\#. },
label={lst:fircs}]{lst/fir.cs}
\vspace{0.5cm}
It is not difficult to notice that all presented implementations use similar steps and differ only in the details resulting from the syntax of individual languages and used data containers. The main conclusion from this observation should be that understanding the operation of DSP algorithms at a basic level will allow you to implemented them in almost any environment or platform without much difficulty.
\vspace{0.5cm}

\subsection{Verification of correct DSP implementation: unit tests}
The concept of \textit{unit testing} is based on creating dedicated code that \textbf{automatically} tests \textbf{unit} code: class, object or function in isolation from other classes in the system; the code is tested only in a given unit of code and all dependencies are simulated by so-called \textit{mocks}  i.e. code that pretends to implement specific functionality, e.g. database or server (also called \textit{stub}, \textit{fake}, \textit{test spy}, \textit{dummy} - the nomenclature in this matter is broad and inconsistent in the literature on the subject). This section provides examples of unit tests in three different environments:
\begin{itemize}[label={\textbullet}]
\item Android Studio IDE for implementation in Java,
\item Chrome web browser using developer tools for implementation in JavaScript,
\item Visual Studio IDE for implementation in C\#.
\end{itemize}

All presented unit tests check the implementation of \lstinline{MyFir} class by creating a new object of this class, performing 500 filtration steps and comparing the responses with those obtained in the GNU Octave environment. The adopted performance error metric is root mean square error (RMSE). The assumed error tolerance is $ 10^{- 10} $. In practice, all the tests presented are characterized by zero error, however, in the case of more complex, non-linear algorithms, minor discrepancies between different environments should be expected. \\

Please note that unit testing in only \textit{start} of the multi-stage software testing process. After performing unit tests, it is necessary to carry out, among others integration tests.
Integration tests check correct operation of many code units to a different extent: from several connected classes to the use of different systems, e.g. database, server, user interfaces. Integration tests based on external systems, i.e. those that use e.g. a database system or application server, are called system tests. Integration tests that also use the user interface are called functional or end-to-end (e2e) tests.

\subsubsection{FIR filter unit test in Java}
Listing \ref{lst:testjava}. shows unit test code for FIR filter written in Java. Test was performed with JUnit framework and Android Studio IDE \cite{junit}. In fig. \ref{fig:ut_android}. is presented user interface of the environment after a successful test.

\lstinputlisting[style=MyJavaStyle, 
caption={ Unit test code for the \lstinline{MyFir} class (Java) in Android Studio IDE. },
label={lst:testjava}]{lst/test.java}

\begin{figure}[H]
\includecgraphics[width=15.0cm]{ut_android.png}
\caption{ Unit test of the \lstinline{MyFir} class (Java) in Android Studio IDE completed successfully. }
\label{fig:ut_android}
\end{figure}
\subsubsection{FIR filter unit test in JavaScript}

Listing \ref{lst:testjs}. shows unit test code for FIR filter written in JavaScript. Tests was performed using Chrome browser and the Unit.js library \cite{unitjs}. A simple HTML document was created in the browser to run the test. In fig. \ref{fig:ut_www}. is presented HTML document and information in the web console after a successful test.

\lstinputlisting[style=MyJavaScriptStyle, 
caption={   Unit test code for the \lstinline{MyFir} class (JavaScript) in Chrome browser. },
label={lst:testjs}]{lst/test.js}

\begin{figure}[H]
\includecgraphics[width=17.0cm]{ut_www2.png}
\caption{  Unit test of the \lstinline{MyFir} class (JavaScript) in Chrome browser completed successfully. }
\label{fig:ut_www}
\end{figure}
\subsubsection{FIR filter unit test in C\#}

Listing \ref{lst:testcs}. shows unit test code for FIR filter written in Java. Test was performed with MSTest V2 framework and Visual Studio IDE \cite{mstest}. In fig. \ref{fig:ut_windows}. is presented user interface of the environment after a successful test.
\vspace{0.5cm}
\lstinputlisting[style=MyCSStyle, 
caption={  Unit test code for the \lstinline{MyFir} class (C\#) in Visual Studio IDE. },
label={lst:testcs}]{lst/test.cs}

\begin{figure}[H]
\includecgraphics[width=15.0cm]{ut_windows.png}
\caption{ Unit test of the \lstinline{MyFir} class (C\#) in Visual Studio IDE completed successfully. }

\label{fig:ut_windows}
\end{figure}

\subsection{Usage of DSP in demo applications}

After performing successful unit tests, we can proceed to integrate  DSP algorithm with other system modules. As part of this example, three demo applications were implemented that integrate FIR filter with the user interface and network communication. User interfaces include a time series graph showing the original test signal and the signal after filtration (updated in real time), information about the sampling frequency and a button to start the demo. Each application allows you to choose between the \textit {mock} server an actual server application using the radio buttons. \textit{Mock} server is simply a test signal generating function while actual server application generates an identical signal with CGI script. \\

Procedure for using filter in all presented environments has following steps:
\begin {enumerate}
\item Obtaining the current sample of test signal based on the internal time counter, depending on the user's choice, from \textit{mock} or server application.
\item \lstinline{Execute} function call from filter object.
\item Charts update.
\item Time counter update.
\end{enumerate}

\subsubsection{Demo of mobile application with the FIR filter}

In listing \ref{lst:demojava}. code of filtration procedure for demo mobile application for Android is presented. This procedure is called by a timer with a sampling period of $t_s$ = 100 ms. The \lstinline{runOnUiThread} function was used to avoid parallel access to the chart collection in case of delays in network communication. To execute requests in blocking (synchronous) mode, the class \lstinline{RequestFuture} from the library Volley was used \cite{volley}. GraphView \cite{graphview} was used to create the chart. In fig. \ref{fig: android}. application GUI is shown.

\lstinputlisting[style=MyJavaStyle, 
caption={ Integration of DSP algorithm with a mobile client application.  },
label={lst:demojava}]{lst/demo.java}

\begin{figure}[H]
\includecgraphics[width=12.0cm]{android.png}
\caption{ GUI of demo mobile application. }
\label{fig:android}
\end{figure}
\subsubsection{Demo of web application with the FIR filter}

In listing \ref{lst:demojs}. code of filtration procedure for demo web application running in the Chrome browser is presented. This procedure is called by a timer with a sampling period of $t_s$ = 100 ms. In order to synchronize filtration process with network communication, the \lstinline{async} modifier and \lstinline{await} operator were used \cite{asyncjs}. The jQuery library was used for network communication \cite{jquery}. Chart.js was used to create the chart \cite{chartjs}. In fig. \ref{fig:www}. application GUI is shown.

\lstinputlisting[style=MyJavaScriptStyle, 
caption={ Integration of DSP algorithm with a web client application.  },
label={lst:demojs}]{lst/demo.js}

\begin{figure}[H]
\includecgraphics[width=12.0cm]{www.png}
\caption{ GUI of demo web application. }
\label{fig:www}
\end{figure}
\subsubsection{Demo of desktop application with the FIR filter}

In listing \ref{lst:democs}. code of filtration procedure for demo Windows desktop application is presented. This procedure is called by a timer  (dedicated for Windows) with a sampling period of $t_s$ = 100 ms. In order to synchronize filtration process with network communication, the \lstinline{async} modifier and \lstinline{await} operator were used \cite{asynccs}. The \lstinline{HttpClient} class was used for network communication \cite{httpclient}. OxyPlot was used to create the chart \cite {oxyplot}. In fig. \ref{fig:windows}. application GUI is shown.

\lstinputlisting[style=MyCSStyle, 
caption={ Integration of DSP algorithm with a desktop client application. },
label={lst:democs}]{lst/demo.cs}

\begin{figure}[H]
\includecgraphics[width=15.0cm]{windows.png}
\caption{ GUI of demo desktop application. }
\label{fig:windows}
\end{figure}
\section{Scenario for the class}

\subsection{Teaching resources}
\begin{tabular}{ l  p{10cm} l }
 Hardware   & \tabitem computer,                                         & \\
            & \tabitem mobile device with Android OS,                    & (optional) \\
            & \tabitem single-board computer Raspberry Pi,               & (optional) \\
 Software   & \tabitem text editor (e.g. Notepad++, Visual Studio Code), & \\
 			& \tabitem Python interpreter,						         & \\
 			& \tabitem GNU Octave,               						 & (optional) \\
 			& \tabitem web server (eg. Lighttpd, Apache),                & (optional) \\
 			& \tabitem Android Studio IDE,                               & (optional) \\
 			& \tabitem web browser (e.g. Chrome, Firefox),               & (optional) \\
 			& \tabitem Visual Studio IDE,             					 & (optional) \\
 			
\end{tabular}

\subsection{Tasks} \label{exercises}

Familiarize yourself with presented examples of DSP design and implementation. Based on the proposed procedure, implement filter with an infinite impulse response (IIR filter) for the IoT server application written in Python. Assume the sampling period $ t_s $ = 10 ms.
\begin{enumerate}
\item Write a script that generates IIR filter parameters and unit test data. Use e.g. the GNU Octave environment or the SciPy library. Use library functions. The filter should pass frequencies less than 10 Hz and stop frequencies greater than 15 Hz. Adopt a bandwidth attenuation of at least 60 dB. Use any type of IIR filter (Chebyshev type I or type II, Butterworth, Bessel, etc.). To obtain data for the unit test, generate an example test signal and filter it.
\item Write your own implementation of digital IIR filter. in Python Do not use library functions - the implementation should be based only on loops, addition, multiplication and operations on arrays.
\item Write a unit test of your own implementation of the IIR filter using e.g. the library \lstinline{unittest} \cite{unittest}. Use previously generated data and filter parameters. Perform the test and make sure it is done correctly.
\item \textbf{(*)} Integrate the algorithm with the IoT system. Write an application containing measurement signal filtration procedure. The measurement signal can come from \textit{mock} or one of the Sense Hat sensors (emulator or physical device). Use timer for synchronization. Application in each step should add: sample number, measurement signal value, filtered signal value to text file. The program should act as a (\textit{daemon}), not as a CGI script.
\end{enumerate}

\bibliographystyle{IEEEtran}
\bibliography{../TemplateLatex/AM_REF} 

\end{document}
