#!/usr/bin/python3
import cgi
import cgitb; cgitb.enable()  # for troubleshooting
import math

print("Content-Type: text/html\n")

x = 2*math.pi*int(cgi.FieldStorage().getvalue('k'))/10
s = math.sin(0.1*x)+math.sin(0.2*x)+math.sin(0.7*x)+math.sin(1.0*x)

print(s)