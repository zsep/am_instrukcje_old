public class MyFir
{
    //!< Array of FIR filter feedforward coefficients
    private double[] feedforward_coefficients;
    //!< List of FIR filter state values
    private List<double> state;    
    /**
      * @brief Initialization of FIR filter algorithm. 
      * @param ffc Array of FIR filter feedforward coefficients.
      * @param st Array of FIR filter input initial state.
      *        Must be the same size as coefficients array.
      */
    public MyFir(double[] fc, double[] s)
    {
        feedforward_coefficients = fc;
        state = new List<double>(s);
    }
    /**
     * @brief Execute one step of the FIR filter algorithm.
     * @param x Input signal.
     * @retval Output [filtered] signal.
     */
    public double Execute(double x)
    {
        // update state
        state.Insert(0, x);
        state.RemoveAt(state.Count - 1);
        // compute output
        double xf = 0.0;
        for (int i = 0; i < state.Count; i++)
        {
            xf += feedforward_coefficients[i] * state[i];
        }
        return xf;
    }
}