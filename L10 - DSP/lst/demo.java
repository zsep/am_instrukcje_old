/** @brief Demo of signal filtering procedure using the FIR filter. */
private void  FilterProcedure() {
    if (k <= sampleMax) {
        // get signal
        final Double x;
        if (signalMock) {
            // from mock object
            x = serverMock.getTestSignal(k);
        } else {
            // from server
            x = server.getTestSignal(k);
        }
        // filter signal
        final Double xf = filter.Execute(x);
        // display data (GraphView)
        runOnUiThread(()-->{
            signal[0].appendData(new DataPoint(k*sampleTime,x), false, sampleMax);
            signal[1].appendData(new DataPoint(k*sampleTime,xf), false, sampleMax);
            chart.onDataChanged(true, true);
        });
        // update time
        k++;
    } else {
        filterTimer.cancel();
        filterTimer = null;
    }
}