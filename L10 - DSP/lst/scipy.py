import math
import numpy as np
from scipy.fftpack import fft, fftshift
from scipy.signal import firwin, lfilter
# sampling frequency
ts = 0.1;  # [s]
fs = 1/ts; # [Hz]

## Test signal --------------------------------------------------------
# frequency components
fcomp = [0.1, 0.2, 0.7, 1.0]; # [Hz]
# signal (in sample domain)
signal = lambda n : (math.sin(2*math.pi*fcomp[0]*(n/fs))+
                     math.sin(2*math.pi*fcomp[1]*(n/fs))+
                     math.sin(2*math.pi*fcomp[2]*(n/fs))+
                     math.sin(2*math.pi*fcomp[3]*(n/fs)))
nvec = list(range(10**4))
xvec = [signal(n) for n in nvec]
# test signal amplitude spectrum
# frequency vector
frange = np.arange(-1/2, 1/2, 1/len(nvec)) # [-]
fxvec = frange*fs                          # [Hz]
# amplitude response
Axvec = abs(fftshift(fft(xvec)))     # [-]
Axvec = [a/len(nvec) for a in Axvec] # [-]

## Filter desired parameters ------------------------------------------
# cut-off frequency
f1 = 0.5 # [Hz]
# frequency at the end transition band
f2 = 0.8 # [Hz]
# transition band length
df = f2 - f1
# stopband attenuation of A [dB] at f2 [Hz]
A = 60 # [dB]
               
## Filter order estimation
# with 'fred harris rule of thumb'
N = round( (fs/df)*A / 22 )

## Filter object with fir1 funcion ------------------------------------
# normalised frequency
w = f1 / (fs/2) # [-]
# low pass filter
H = firwin(N+1, w, pass_zero='lowpass') # number of taps - NOT order!

##Filter frequency response -------------------------------------------
# no. of samples
n = 10**4; # [-]
# frequency vector
frange = np.arange(-1/2, 1/2, 1/n) # [-]
fhvec = frange*fs                  # [Hz]
# amplitude response
Ahvec = abs(fftshift(fft(H,n=n)))  # [-] impulse response fft 
Ahvec = [20*math.log10(a + 1.0e-15) for a in Ahvec] # [dB]

## Test signal filtration ---------------------------------------------
xfvec = lfilter(H,1,xvec)
# filtered signal amplitude spectrum
# amplitude response
Axfvec = abs(fftshift(fft(xfvec)))     # [-]
Axfvec = [a/len(nvec) for a in Axfvec] # [-]
