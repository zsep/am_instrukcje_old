/** @brief Demo of signal filtering procedure using the FIR filter. */
async function FilterProcedure() {
    if( k <= samplesMax ){
        // get signal
        if(signalLocal) {
            // from TestSignal object
            x = serverMock.getTestSignal(k);
        } else {
            // from server 
            x = await server.getTestSignal(k)
        }
        // filter signal
        xf = filter.Execute(x);
        // display data (Chart.js)
        signal[1].push(x);
        signal[0].push(xf);
        chart.update();
        // update time
        k++;
    } else {
        clearInterval(filterTimer);
        filterTimer = null;
    }
}