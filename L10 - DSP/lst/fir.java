public class MyFir {
    //!< Array of FIR filter feedforward coefficients
    private Double[] feedforward_coefficients;
    //!< List of FIR filter state values
    private List<Double> state;

    /**
     * @brief Initialization of FIR filter algorithm.
     * @param ffc Array of FIR filter feedforward coefficients.
     * @param st Array of FIR filter input initial state.
     *        Must be the same size as coefficients array.
     */
    public MyFir(Double[] ffc, Double[] st)
    {
        feedforward_coefficients = ffc;
        state = new ArrayList<>(Arrays.asList(st));
    }
    /**
     * @brief Execute one step of the FIR filter algorithm.
     * @param x Input signal.
     * @retval Output [filtered] signal.
     */
    public Double Execute(Double x)
    {
        // update state
        state.add(0, x);
        state.remove(state.size() - 1);
        // compute output
        Double xf = 0.0;
        for (int i = 0; i < state.size(); i++) {
            xf += feedforward_coefficients[i] * state.get(i);
        }
        return xf;
    }
}