pkg load signal
% sampling frequency
ts = 0.1;  % [s]
fs = 1/ts; % [Hz]

%% Test signal --------------------------------------------------------
% frequency components
fcomp = [0.1, 0.2, 0.7, 1.0]; % [Hz]
% signal (in sample domain)
signal = @(n)( sin(2*pi*fcomp(1)*(n/fs)) + sin(2*pi*fcomp(2)*(n/fs)) + ...
               sin(2*pi*fcomp(3)*(n/fs)) + sin(2*pi*fcomp(4)*(n/fs)) );
nvec = 0 : 10^4 - 1;
xvec = signal(nvec)';   
% test signal amplitude spectrum
% frequency vector
frange = (-1/2 : 1/length(nvec) : 1/2-1/length(nvec)); % [-]
fxvec = frange*fs;                                     % [Hz]
% amplitude response
Axvec = abs(fftshift(fft(xvec,length(nvec)))); % [-]
Axvec = Axvec / length(nvec);
        
%% Filter desired parameters ------------------------------------------
% cut-off frequency
f1 = 0.5; % [Hz]
% frequency at the end transition band
f2 = 0.8; % [Hz]
% transition band length
df = f2 - f1;
% stopband attenuation of A [dB] at f2 [Hz]
A = 60; % [dB]
               
%% Filter order estimation --------------------------------------------
% with 'fred harris rule of thumb'
N = round( (fs/df)*A / 22 );

%% Filter object with fir1 funcion ------------------------------------
% normalised frequency
w = f1 / (fs/2); % [-]
% low pass filter
H = fir1(N, w, 'low');

%% Filter frequency response ------------------------------------------
% no. of samples
n = 10^4; % [-]
% frequency vector
frange = (-1/2 : 1/n : 1/2-1/n); % [-]
fhvec = frange*fs;               % [Hz]
% amplitude response
Ahvec_v1 = abs(freqz(H, 1, 2*pi*frange)); % [-] frequency response
Ahvec_v1 = 20*log10(Ahvec_v1); % [dB]
Ahvec_v2 = abs(fftshift(fft(H,n)));       % [-] impulse response fft 
Ahvec_v2 = 20*log10(Ahvec_v2); % [dB]

%% Test signal filtration ---------------------------------------------
xfvec = filter(H,1,xvec);
% filtered signal amplitude spectrum
% amplitude response
Axfvec = abs(fftshift(fft(xfvec,length(nvec)))); % [-]
Axfvec = Axfvec / length(nvec);
