/** @brief Demo of signal filtering procedure using the FIR filter. */
private async void FilterProcedure(object sender, EventArgs e)
{
    if (k <= samplesMax) {
        // get signal
        double x;
        if (signalMock) {
            // from mock object
            x = serverMock.getTestSignal(k);
        } else {
            // from server
            x = await server.getTestSignal(k);
        }
        // filter signal
        double xf = filter.Execute(x);
        // display data (OxyPlot)
        (chart.Series[0] as LineSeries).Points.Add(new DataPoint(k*sampleTime,x));
        (chart.Series[1] as LineSeries).Points.Add(new DataPoint(k*sampleTime,xf));
        chart.InvalidatePlot(true);
        // update time
        k++;
    } else {
        filterTimer.Stop();
        filterTimer = null;
    }
}