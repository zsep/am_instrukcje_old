\documentclass[11pt, a4paper]{article}

\usepackage[]{../TemplateLatex/am} % [pl] or []

%%% Definitions %%%
\university{Poznań University of Technology}
\institute{Institute of Robotics and Machine Intelligence}
\department{Division of Control and Industrial Electronics}
\lab{Mobile and embedded applications for Internet of Things}
\author{Dominik Łuczak, Ph.D.; Adrian Wójcik, M.Sc.}
\email{Dominik.Luczak@put.poznan.pl \\ Adrian.Wojcik@put.poznan.pl}


\maintitle{Final task \\ System IoT}

\comment{Teaching materials for laboratory}
%%
%%
\setlength\parindent{0pt}
\begin{document}

%% First page %%
\mainpage

%%
%%

\section{Cel realizacji zadania}

\subsection*{Knowledge}
The goal of the task is to familiarize yourself with:
\begin{itemize}[label={\textbullet}]
\item structure of a custom IoT system,
\item network communication and client-server architecture in IoT system,
\item REST architecture on a practical example,
\item available tools and technologies for implementation of IoT system elements,
\item role of embedded measurement and control systems in IoT.
\end{itemize}

\subsection*{Skills}
The goal of the task is to acquire skills in:
\begin{itemize}[label={\textbullet}]
\item implementing the IoT server application,
\item handling serial communication between an IoT server and an embedded measurement and control systems,
\item implementing mobile client application for IoT system,,
\item implementing web client application for IoT system,
\item implementing desktop client application for IoT system,,
\item handling network communication between a server and a client of IoT system,
\item implementing automatic tests.
\end{itemize}

\subsection*{Social competences}
The goal of the task is to develop proper attitudes: 
\begin{itemize}[label={\textbullet}]
\item using systematic software development techniques and methodologies,
\item using tools to facilitate team work,
\item correct and systematic testing of the developed software,
\item correct documentation of the developed system functionality and tests results,
\item choosing the right technology and programming tools for the given problem,
\item strengthening understanding and awareness of the importance of non-technical aspects and effects of the engineer's activities, and the related responsibility for the decisions taken.
\end{itemize}

\section{Description of final task}
The final task is development and integration of Internet of Things (IoT) system with server and clients applications prepared during the course. The task should be carried out in groups of not more than 4 students. The task evaluation will be based on the presentation of the developed system (\ref{P} and the report documenting the development process and results of the system tests (\ref{R}.

\subsection{IoT system structure}
Fig. \ref{fig:scheme}. shows schematic diagram of an IoT system. System should consist of five basic elements:
\begin{enumerate}
\item Embedded  measurement and control system, i.e. a microprocessor system equipped with min. one sensor and min. one control element - e.g. \textbf{Sense Hat} board or a dedicated system based on a development kit with a STM32 microcontroller. This element of the system have to enable reading of sensors and control of outputs.
\item Single-board computer (SBC) equipped with network (e.g. Ethernet, WiFi) and serial interfaces (e.g. I2C, SPI, UART) - e.g. \textbf{Raspberry Pi}. This element of the system have to mediate between an embedded system and client applications using a web server.
\item A mobile device with \textbf{Android}, iOS or Windows 10 Mobile operating system, with a dedicated client application.
\item A personal computer with \textbf{Windows}, \textbf{Linux} or macOS operating system, with a dedicated client application.
\item A web browser (launched in any environment) e.g. \textbf{Firefox} or \textbf{Chrome}, allowing use of a dedicated web client application.
\end{enumerate}
All client applications have to contain a graphical user interface (GUI) allowing for interaction with an embedded system, i.e. view of sensors state and control of outputs. All components can be replaced by virtual machines or emulators.

\begin{figure}[H]
\includecgraphics[width=15.0cm]{scheme.png}
\caption{ Schematic diagram of structure of an IoT system.}
\label{fig:scheme}
\end{figure}

\subsection{IoT system specification}
The developed IoT system \underline{must} meet the following \textbf{basic requirements}:
\begin{enumerate}
\item Web server allows to download measurement data from all sensors available in an embedded system and send control commands to all outputs.
\item Web server allows to send measurement data to all three client applications and receives control commands from all three client applications.
\item All three client applications allow the measurement data to be viewed using the \ul{dynamically generated} user interface (e.g. in the form of a list or table).
\item All three client applications allow the measurement data to be viewed using a time graph.
\item All three client applications allow measurement with sample time less than or equal to 1000 ms.
\item GUI of all three client applications contains information about units of measured quantities.
\item All three client applications allows to control individual outputs in the full available control range.
\item All three client applications allow configuration of network communication (server address and port) and data acquisition (sample time and maximum number of saved measurement points).
\end{enumerate}

The developed IoT system \underline{should} meet the following \textbf{additional requirements}:
\begin{enumerate}
\item System implementation use Representational state transfer (REST) architecture.
\item Implementation of client applications maintains the same application architecture and method names in all three environments.
\item All source code is commented in compliance with a common standard (e.g. Doxygen).
\item SBC contains two system shell scripts - for configuring and deconfiguring system, web server and server applications.
\item All three client applications allow measurement with sample time less than or equal to 100 ms.
\item The server and client applications allows to view all available physical quantities measured by sensors (e.g. LSM9DS1 sensor allows to measure not only RPY angles but also magnetic induction, angular and linear accelerations; temperature measurement is possible from both the LPS25H and HTS221 sensor, etc.)
\item A version control system (e.g. Git) was used to implement the task.
\item Automatic tests were used to test the developed system features.
\item The mobile application uses a design pattern that ensures the separation of the user interface from the application logic (e.g. Model-View-Controller, Model-View-Presenter, Model-View-VieModel) \cite{pattern1}\cite{pattern2}.
\item The desktop application uses a design pattern that ensures the separation of the user interface from the application logic (e.g. Model-View-Controller, Model-View-Presenter, Model-View-VieModel) \cite{pattern3}.
\item The system uses digital signal processing of measurement data, e.g. in the form of digital filters.
\item Encrypted communication with the server is possible using the HTTPS protocol, e.g. using OpenSSL \cite{ssl1}\cite{ssl2}.
\end{enumerate}

\subsection{System presentation} \label{P}

Presentation of developed IoT system should demonstrate that assumed requirements were met. System can be presented in three ways:
\begin{itemize}[label={\textbullet}]
\item On-line presentation via videoconference with display of presenter's or camera's desktop.
\item Off-line presentation via prerecorded material - in a convention similar to on-line presentation.
\item Presentation in the laboratory - \ul{one} representative of a group performs a live presentation in the laboratory room.
\end{itemize}

The presentation is graded based on percentage effectiveness of meeting given requirements. In order to obtain a positive grade, \ul{all} basic requirements must be met. If the basic requirements are met, the grade expressed as a percentage is determined according to the formula (\ref{eq0}):

\begin{equation}\label{eq0}
P = \left(1 + \frac{WD}{8}\right)\cdot50\%
\end{equation}


\begin{tabular}{ r c c  p{13cm} }
where:  & $P$  & - & presentation grade in percents, \\
 		& $WD$ & - & number of fulfilled additional requirements. \\
\end{tabular}
\vspace{0.5cm}
\subsection{Report of system development} \label{R}

Report documenting implementation of the final task should consist of four main sections:

\begin{enumerate}
\item \textbf{Specification description}: i.e. which system requirements were intended to be met and what additional assumptions were made. This section should also specify which target platforms were used.
\item \textbf{System implementation}: should be divided into four subsections:
\begin{enumerate}
\item Server applications,
\item Mobile client application,
\item Web client application,
\item Desktop client application.
\end{enumerate}
This section \ul{should not} contain all the source code, but only a description of used software tools, adopted application architectures, designed interfaces and instructions for using client applications. A convenient way to present the results of implementation are Unified Modeling Language (UML) diagrams.
\item \textbf{Results of tests and system integration}: description of the adopted methodology for testing individual features and the results of performed tests.
\item \textbf{Conclusions and summary}: description of which requirements and assumptions have been met and to what extent. 
\end{enumerate}

The document should be prepared in accordance with the editorial guidelines of \cite{szablon_raportu}.

\subsection{Organization of source code} \label{SC}

All source code - including whole projects in used IDEs - must be passed on to laboratory instructor after task is completed. Uploaded files should be organized as follows:
\begin{itemize}[label={\textbullet}]
\item Archive \lstinline{server_app.zip} containing all sources of server applications and scripts for configuring/deconfiguring SBC.
\item Archive \lstinline{client_app_desktop.zip} containing all sources of the desktop client application.
\item Archive \lstinline{client_app_mobile.zip} containing all sources of the mobile client application.
\item Archive \lstinline{client_app_web.zip} containing all sources of the web client application.
\item Document \lstinline{report_en.pdf} containing the report.
\end{itemize}
All files should be packed into an archive named \lstinline{System_IoT_Gx.zip} where \lstinline {x} is the group identifier (\lstinline{A}-\lstinline{Z}).

\subsection{Teaching resources}
\begin{tabular}{ l  p{10cm} l }
 Hardware   & \tabitem computer,                               & \\
            & \tabitem mobile device with Android OS,            & \\
            & \tabitem single-board computer Raspberry Pi,             & \\
 Software   & \tabitem SBC Raspberry Pi virtual machine,                   & \\ 
            & \tabitem mobile device with Android OS emulator, & \\ 
            & \tabitem SSH client (eg. Bitwise SSH Client), 			 & \\ 
            & \tabitem text editor (eg. Notepad++, Visual Studio Code), & \\       						 
 			& \tabitem web server (eg. Lighttpd, Apache),                & \\
 			& \tabitem Android Studio IDE,                               & \\
 			& \tabitem web browser (eg. Chrome, Firefox),   & \\
 			& \tabitem Visual Studio IDE,             					 & \\
 			& \tabitem Python interpreter,						         & \\
 			& \tabitem GNU Octave.       								 & \\
 			
\end{tabular}
\vspace{0.5cm}

You can use any software and hardware tools that will allow you to perform task according to given specification. Suggested tools are based on  solutions presented during laboratory classes.

\bibliographystyle{IEEEtran}
\bibliography{../TemplateLatex/AM_REF} 

\end{document}
