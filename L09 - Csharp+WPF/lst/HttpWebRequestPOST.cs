HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
// POST request data 
var requestData = "filename=chartdata";
byte[] byteArray = Encoding.UTF8.GetBytes(requestData);
// POST request configuration
request.Method = "POST";
request.ContentType = "application/x-www-form-urlencoded";
request.ContentLength = byteArray.Length;
// Wrire data to request stream
Stream dataStream = request.GetRequestStream();
dataStream.Write(byteArray, 0, byteArray.Length);
dataStream.Close();