public class SimpleViewModel : INotifyPropertyChanged
{
    #region Properties
    private string input; // privte FIELD
    public string Input   // public PROPERTY with 'get' and 'set'
    {
        get
        {
            return input;
        }
        set
        {
            input = value;
            OnPropertyChanged("Input");
        }
    }
    #endregion
    ...
    #region PropertyChanged
    public event PropertyChangedEventHandler PropertyChanged;

    /**
      * @brief Simple function to trigger event handler
      * @params propertyName Name of SimpleViewModel property as string
      */
    protected void OnPropertyChanged(string propertyName)
    {
        PropertyChangedEventHandler handler = PropertyChanged;
        if (handler != null) 
            handler(this, new PropertyChangedEventArgs(propertyName));
    }
    #endregion
}