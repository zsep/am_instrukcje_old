JObject jObj = JObject.Parse(jsonText);
var temp = (string)jObj.Descendants()
                    .OfType<JProperty>()
                    .Where(p => p.Name == "temp")
                    .First()
                    .Value;