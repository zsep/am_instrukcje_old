using (HttpClient client = new HttpClient())
{
    // POST request data
    var requestDataCollection = new List<KeyValuePair<string, string>>();
    requestDataCollection.Add(new KeyValuePair<string, string>("key", "value"));
	...
    var requestData = new FormUrlEncodedContent(requestDataCollection);
    // Sent POST request
    var result = await client.PostAsync(uri, requestData);
    // Read response content
    responseText = await result.Content.ReadAsStringAsync();
}