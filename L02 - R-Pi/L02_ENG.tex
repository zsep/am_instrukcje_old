\documentclass[11pt, a4paper]{article}

\usepackage[]{../TemplateLatex/am} % [pl] or []

%%% Definitions %%%
\university{Poznań University of Technology}
\institute{Institute of Robotics and Machine Intelligence}
\department{Division of Control and Industrial Electronics}
\lab{Mobile and embedded applications for Internet of Things}
\author{Dominik Łuczak, Ph.D.; Adrian Wójcik, M.Sc.}
\email{Dominik.Luczak@put.poznan.pl \\ Adrian.Wojcik@put.poznan.pl}

\maintitle{ Server IoT \\ Raspberry Pi}

\comment{Teaching materials for laboratory}
%%
%%

\begin{document}

%% First page %%
\mainpage

%%
%%

\section{Goal}

\subsection*{Knowledge}
The aim of the course is to familiarize yourself with: 
\begin{itemize}[label={\textbullet}]
\item the basics of the Linux (Raspbian) operating system,
\item the basics of \textit{bash} system shell syntax,
\item basics of Python3 language syntax.
\end{itemize}

\subsection*{Skills}
The aim of the course is to acquire skills in:
\begin{itemize}[label={\textbullet}]
\item configuring network interfaces on Linux,
\item creating CLI applications using bash system shell scripts,
\item creating CLI applications using C and C++,
\item creating a CLI application using Python,
\item operating the digital inputs and outputs on a SBC (Raspberry Pi),
\item operating the PWM modules on a SBC (Raspberry Pi),
\item operating the UART, I2C and SPI interfaces on a SBC (Raspberry Pi) (\textit{TODO}),
\item WWW server configuration in Linux,
\item use of the CLI application in server-side PHP scripts,
\end{itemize}

\subsection*{Social competences}
The aim of the course is to develop proper attitudes:
\begin{itemize}[label={\textbullet}]
\item strengthening the understanding of the role and application of network communication in IT systems and related security issues,
\item strengthening understanding and awareness of the importance of non-technical aspects and effects of the engineer's activities, and the related responsibility for the decisions taken,
\item choosing the right technology and programming tools for the given problem,
\item testing developed IT system.
\end{itemize}

\section{Final Task}
% fixed component
\input{../TemplateLatex/POLECENIA_KONCOWE_ENG.tex}

\section{Prepare to course}

\subsection{Know the safety rules}
% fixed component
\input{../TemplateLatex/BHP_ENG.tex}

\subsection{Introduction to Raspberry Pi}
%% -- BEGIN RPi introduction ----------------------------------------------------------------
Raspberry Pi (RPi) is a series of single-board computers (SBC), i.e. devices that contain a microprocessor(s), memory and input/output devices on single printed circuit. This device is based on an ARM family processor. Raspberry Pi has no hard disk - to load the operating system and store data, it offers a connector for microSD cards or USB flash drives. In addition, it is equipped with a connector with GPIO and popular low-level electronic interfaces - UART, I2C and SPI \cite{rpidoc}. It is a very popular platform both in computer science didactics and rapid prototyping. An important feature of Raspberry Pi is also the extensive and active community of users \cite{rpicomm}, which results in a wide selection of available applications and programming libraries. The recommended (but by no means the only!) operating system for RPi is the Raspbian system based on Debian Linux distributions. 
%% -- END RPi introduction ------------------------------------------------------------------

\subsection{Configuration of network interfaces}
%% -- BEGIN ifconfig ------------------------------------------------------------------------
Raspberry Pi allows you to connect a keyboard and mouse (via USB) and a monitor (via HDMI). However, it is also equipped with network interfaces: Ethernet (\textbf{eth0}) and WiFi (\textbf{wlan0}). For this reason, a much more common way of using RPi is communication via the SSH (secure shell) protocol, which is used to connect to computers remotely using network interfaces. The correct configuration of network interfaces is therefore necessary for the effective use of the platform.

\bigbreak

The basic command for configuring network interfaces is \lstinline{ifconfig} \cite{ifconfig} (not to be confused with the corresponding command in Windows - \lstinline{ipconfig}!).

\bigbreak

In order to obtain a permanent (i.e. saved after restarting the device), \textit{static} IP address, you need to edit the system file \lstinline{/etc/dhcpcd.conf} and add the configuration according to the pattern presented in the file (see: listing \ref{lst:dhcpcdconf}).

\bigbreak

\begin{lstlisting}[style=MyBashStyle, label={lst:dhcpcdconf},
caption={ File /etc/dhcpcd.conf - static IP of \textbf{eth0} interface. }] 	
interface eth0
static ip_address=192.168.1.15
static routers=192.168.1.1
static domain_name_servers=8.8.8.8
\end{lstlisting}

\bigbreak

The use of WiFi requires additional configuration in the form of adding the SSID (service set identifier) and password in the file \lstinline{/etc/wpa_supplicant/wpa_supplicant.conf} - example in the listing \ref{lst:wpa}.

\begin{lstlisting}[style=MyBashStyle, label={lst:wpa},
caption={ File /etc/wpa\_supplicant/wpa\_supplicant.conf - Wireless network SSID and password in room M323. }] 	
network={
        ssid="M323_01"
        psk="labM32301"
}
\end{lstlisting}

\begin{tcolorbox}[colback=purple,coltext=black]
\textbf{NOTE!} Please note that establishing a connection between RPi and another device requires that at least one network interface of each devices is in common network!
\end{tcolorbox}
%% -- END ifconfig --------------------------------------------------------------------------

\subsection{Command line applications (CLIs)}
%% -- BEGIN CLI -----------------------------------------------------------------------------
The two basic variants of the Raspbian system are \cite{raspbian}:
\begin{itemize}[label={\textbullet}]
\item Desktop version - equipped with a graphic user interface (with the option to disable it),
\item Lite version (\textit{headless}) - based only on the command line (command line interpreter/interface, CLI).
\end{itemize}
As part of the course, the Raspbian system will be operated only from the command line. Therefore, you will need to acquire skills to create and use applications launched from the command line. Three different tools will be used for this purpose:
\begin{itemize}[label={\textbullet}]
\item \textbf{bash} system shell scripts \cite{bashdoc},
\item interpreted language \textbf{Python} (version 3.5) \cite{pythondoc},
\item programming language \textbf{C ++} \cite{c11doc} with the \textbf{g++} compiler \cite{gccdoc}.
\end{itemize}

\subsubsection{bash}
Bash is the system shell of the UNIX system. It is the default shell in most GNU/Linux distributions and on macOS. On the listing. \ref{lst:bash1} simple  "Hello World" script is presented. The first line of the script is the path of the appropriate interpreter. On the listing \ref{lst:bash2}. is presented an example of a script that handles the script's input arguments using the \textit{getopt} method \cite{getoptdoc}.

\lstinputlisting[style=MyBashStyle, 
caption={ Bash ,,Hello World''. },
label={lst:bash1}]{lst/cli_examples/bash/bash_HelloWorld.sh}

\lstinputlisting[style=MyBashStyle, 
caption={ Bash - \textit{getopt} function example. },
label={lst:bash2}]{lst/cli_examples/bash/bash_args.sh}

\begin{tcolorbox}[colback=purple,coltext=black]
\textbf{NOTE!} To enable the script to be executed, you must grant it permission with the \lstinline {chmod +x script_name.sh} command.
\end{tcolorbox}

\begin{tcolorbox}[colback=purple,coltext=black]
\textbf{NOTE!} When editing Linux scripts from another operating system (e.g. Windows), make sure that the character encoding is appropriate for UNIX systems. Source code editors (e.g. Notepad++, VS Code etc.) allow you to choose proper configurations.
\end{tcolorbox}

Calculations in Bash are done ONLY on integers and there is no overflow control. If a given problem requires extensive operations on floating point numbers, then Bash is not an adequate tool to solve it. For simple mathematical calculations involving floating point numbers, the calculator application \lstinline{bc} \cite{bcdoc} can be used. On the listing \ref{lst:bashbc}. an example of using the \lstinline{bc} calculator to perform a linear function in the Bash script is shown.

\lstinputlisting[style=MyBashStyle, 
caption={ Bash - \lstinline{bc} calculator app example. },
label={lst:bashbc}]{lst/cli_examples/bash/bash_bc_example.sh}

\bigbreak

\subsubsection{Python}
Python is a popular interpreted language (i.e. a language that is usually implemented using an interpreter, not a compiler) of general purpose, whose popularity is the result of, above all, the extremely extensive standard library. The use of Python requires the installation of its interpreter on the system. In the listing \ref{lst:py1}. simple application ,,Hello World'' is presented. The first line of the script is the path of the appropriate interpreter. In the listing \ref{lst:py2}. is an example of a script that handles the script's input arguments using the \textit{getopt} method \cite{getoptpydoc}. 

\lstinputlisting[style=MyPythonStyle, 
caption={ Python ,,Hello World''. },
label={lst:py1}]{lst/cli_examples/python/python_HelloWorld.py}

\lstinputlisting[style=MyPythonStyle, 
caption={ Python - \textit{getopt} function example. },
label={lst:py2}]{lst/cli_examples/python/python_args.py}

\subsubsection{C++}
C++ is a multi-paradigm general-purpose programming language. Its basic property is a high level of independence from the hardware platform. An important feature is also compatibility with the C language - C++ libraries can be written in C. The use of applications written in C++ requires compilation of source code, and therefore installation of the compiler on the system. On the listing \ref{lst:cpp1}. simple application ,,Hello World'' is presented. Listing \ref{lst:gcc1}. presents a script for compiling source code using g++ compiler. On the listing \ref{lst:cpp2}. an example of a program handling input arguments using the \textit{getopt} method is presented \cite{getoptdoc} .

\lstinputlisting[style=MyCppStyle, 
caption={ C++ ,,Hello World''.  },
label={lst:cpp1}]{lst/cli_examples/cpp/cpp_HelloWorld.cpp}

\lstinputlisting[style=MyBashStyle, 
caption={ Build ,,Hello World'' using g++ compiler. },
label={lst:gcc1}]{lst/cli_examples/cpp/cpp_HelloWorld_CompilationScript.sh}

\lstinputlisting[style=MyCppStyle, 
caption={ C++ - \textit{getopt} function example. },
label={lst:cpp2}]{lst/cli_examples/cpp/cpp_args.cpp}
%% -- END CLI -------------------------------------------------------------------------------

\subsection{Peripheral devices}
%% -- BEGIN Peripheral ----------------------------------------------------------------------
RPi is equipped with a 40-pin connector for general purpose digital input/output (GPIO), 2 PWM channels, and interfaces: UART, I2C and SPI (fig. \ref{fig: pinout}).

\begin{figure}[H]
\includecgraphics[width=13.0cm]{pinout.png}
\caption{ Raspberry Pi low-level socket pinout. }
\label{fig:pinout}
\end{figure}

On Linux systems, hardware access is analogous to reading/writing to files - i.e. files represent hardware. It is possible to operate the peripherals by executing the commands \lstinline{echo} (write) and \lstinline{cat} (read) respectively. In practice, however, there are available programming libraries that provide a convenient API for basic operations related to a given periphery, e.g. WiringPi \cite{wiringpi}.

\bigbreak

The examples presented below were implemented using external elements as in the diagram in Fig. \ref{fig:sch}.

\begin{figure}[H]
\includecgraphics[width=15.0cm]{sch.png}
\caption{ Electrical diagram of external elements. }
\label{fig:sch}
\end{figure}

\subsubsection{GPIO}
Below are three sets of listings for GPIO operations: bash system shell scripts with direct file access, Python scripts using the RPi.GPIO library and C++ programs using the WiringPi library.

\bigbreak

Listings \ref{lst:gpiosh1}.-\ref{lst:gpiosh5}. shows respectively: output initialization, write (1/0), digital output deinitialization and simple application example.

\lstinputlisting[style=MyBashStyle, 
caption={ GPIO operations using bash system shell - output initialization. }, 
label={lst:gpiosh1}]{lst/gpio_examples/bash/gpio_output_init.sh}

\lstinputlisting[style=MyBashStyle, 
caption={ GPIO operations using bash system shell - write 1 (\textit{high}). }, 
label={lst:gpiosh2}]{lst/gpio_examples/bash/gpio_output_set.sh}

\newpage

\lstinputlisting[style=MyBashStyle, 
caption={ GPIO operations using bash system shell - write 0 (\textit{high}).}, 
label={lst:gpiosh3}]{lst/gpio_examples/bash/gpio_output_reset.sh}

\lstinputlisting[style=MyBashStyle, 
caption={ GPIO operations using bash system shell - output deinitialization. }, 
label={lst:gpiosh4}]{lst/gpio_examples/bash/gpio_output_deinit.sh}

\lstinputlisting[style=MyBashStyle, 
caption={ GPIO operations using bash system shell  - simple application example: LED toggle. }, label={lst:gpiosh5}]{lst/gpio_examples/bash/gpio_output_example.sh}

\bigbreak

Listings \ref{lst:gpiosh6}.-\ref{lst:gpiosh8}. shows respectively: input initialization, input readout and application example. Deinitialization is identical regardless of GPIO direction.

\lstinputlisting[style=MyBashStyle, 
caption={ GPIO operations using bash system shell - input initialization. }, 
label={lst:gpiosh6}]{lst/gpio_examples/bash/gpio_input_init.sh}

\lstinputlisting[style=MyBashStyle, 
caption={ GPIO operations using bash system shell - input read. }, 
label={lst:gpiosh7}]{lst/gpio_examples/bash/gpio_input_read.sh}

\lstinputlisting[style=MyBashStyle, caption={ GPIO operations using bash system shell  - simple application example: rising edge detection. }, label={lst:gpiosh8}]{lst/gpio_examples/bash/gpio_input_example.sh}

\bigbreak

%% Python
On the listings \ref{lst:gpiopy1}.-\ref{lst:gpiopy3}. examples of a simple application are presented for a digital output, a digital input, and output control by input edge, using the RPi.GPIO library.

\lstinputlisting[style=MyPythonStyle, 
caption={ GPIO operations using Python - LED toggle. }, 
label={lst:gpiopy1}]{lst/gpio_examples/python/gpio_output_example.py}

\lstinputlisting[style=MyPythonStyle, 
caption={ GPIO operations using Python - rising edge detection. }, 
label={lst:gpiopy2}]{lst/gpio_examples/python/gpio_input_example.py}

\lstinputlisting[style=MyPythonStyle, 
caption={ GPIO operations using Python - LED toggle after edge detection. }, 
label={lst:gpiopy3}]{lst/gpio_examples/python/gpio_example.py}

\bigbreak

For programs written in C++, an external WiringPi library was used. It is worth noting that it is also (\textit{unofficially}) available for Python. When you install this library, a set of additional tools is available, including the \textbf{gpio} application. It enables GPIO support directly from the terminal level in a way significantly simpler than direct files read/write. When using WiringPi tools, it is always worth making sure which GPIO numbering system we use - this is facilitated by the \lstinline{gpio readall} command. Result for RPi model 3B is shown in fig \ref{fig:wpi}.

\begin{figure}[H]
\includecgraphics[width=10.5cm]{gpio.png}
\caption{ WiringPi connector numbering - RPi model 3B. }
\label{fig:wpi}
\end{figure}

On the listings \ref{lst:gpiocpp1}.-\ref{lst:gpiocpp3}. examples of a simple application are presented for a digital output, a digital input and and output control by input edge, using C++ with the WiringPi library.

\lstinputlisting[style=MyCppStyle, 
caption={ GPIO operations using C++ - LED toggle.  },
label={lst:gpiocpp1}]{lst/gpio_examples/cpp/gpio_output_example.cpp}

\lstinputlisting[style=MyCppStyle, 
caption={ GPIO operations using C++ - rising edge detection. },
label={lst:gpiocpp2}]{lst/gpio_examples/cpp/gpio_input_example.cpp}

\lstinputlisting[style=MyCppStyle,
caption={ GPIO operations using C++ - LED toggle after edge detection. }, 
label={lst:gpiocpp3}]{lst/gpio_examples/cpp/gpio_example.cpp}

\subsubsection{PWM}
RPi is equipped with 2 PWM channels, each of which can be mapped to one of 2 different pins. On the listing \ref{lst:pwmsh1}. presents a Bash system shell script using the \textbf{gpio} application to configure PWM1 - the output works with a constant frequency of 50 Hz and duty specified by the user with the script's input argument (validation omitted). On the listing. \ref{lst:pwmsh2} sample application - linear periodic change of duty.

\lstinputlisting[style=MyBashStyle, 
caption={ PWM operations using bash system shell and \textbf{gpio} app - duty setting. },
label={lst:pwmsh1}]{lst/pwm_examples/bash/pwm_setDuty_example.sh}

\lstinputlisting[style=MyBashStyle,
caption={ PWM operations using bash system shell and \textbf{gpio} app - periodic duty change. }, 
label={lst:pwmsh2}]{lst/pwm_examples/bash/pwm_example.sh}

\bigbreak

On the listings \ref{lst:pwmpy1}.-\ref{lst:pwmpy2}. are presented similar applications  in Python using the RPi.GPIO library.

\lstinputlisting[style=MyPythonStyle, 
caption={ PWM operations using Python - duty setting. }, 
label={lst:pwmpy1}]{lst/pwm_examples/python/pwm_setDuty_example.py}

\lstinputlisting[style=MyPythonStyle, 
caption={ PWM operations using Python - periodic duty change. }, 
label={lst:pwmpy2}]{lst/pwm_examples/python/pwm_example.py}

\bigbreak

On the listings \ref{lst:pwmcpp1}.-\ref{lst:pwmcpp1}. are presented similar applications in C++ using the WiringPi library.

\lstinputlisting[style=MyCppStyle, 
caption={ PWM operations using C++ and WiringPi - duty setting. }, 
label={lst:pwmcpp1}]{lst/pwm_examples/cpp/pwm_setDuty_example.cpp}

\lstinputlisting[style=MyCppStyle, 
caption={ PWM operations using C++ and WiringPi - periodic duty change. }, 
label={lst:pwmcpp2}]{lst/pwm_examples/cpp/pwm_example.cpp}

%% TODO
%%\subsubsection{I2C}

%% TODO
%%\subsubsection{SPI}

%% TODO
%%\subsubsection{UART}

%% -- END Peripheral ------------------------------------------------------------------------

\subsection{Server configuration - Lighttpd}
%% -- BEGIN Lighhtpd ------------------------------------------------------------------------
Using RPi as the Internet of Things (IoT) server requires configuration of web server and PHP interpreter. During course we will use the Lighttpd application - an open source web server dedicated to platforms with limited hardware resources, while maintaining compliance with standards, security and flexibility \cite{lighttpd}. Laboratory kits (and virtual machines) \textbf{do not require additional installation}. You can install the Lighttpd application on the Raspbian system with the \lstinline{apt-get install lighttpd} command. To enable the use of PHP scripts on the server, you must install the necessary software with the command \lstinline{apt-get -y install php7.x-fpm php7.x}, where \textbf{x} is the version number of PHP7 (currently 0-4).

\bigbreak

The basic server configuration is available in the file \lstinline{/etc/lighttpd/lighttpd.conf} (listing \ref{lst:lighttpdconf}.). When modifying (overwriting) configuration files, it's a good practice to make a backup copy of them beforehand: \\
\lstinline{cp filename.conf filename.conf.bak}, \\
to restore defaults later if needed: \\
\lstinline{cp filename.conf.bak filename.conf}. \\
Working with the server, it is worth modifying the work path (\textit{server.document-root)} to one that we will have full access to from the SSH client. Important information is also the username (\textit{server.username}) and group name (\textit {server.groupname}). This is key information in the context of giving the server appropriate permissions.

\lstinputlisting[style=MyBashStyle, 
caption={ Default contents of  configuration file /etc/lighttpd/lighttpd.conf }, 
label={lst:lighttpdconf}]{lst/server_examples/lighhtpd.conf}

If we want the server to be able to execute programs and scripts interacting with the file system and hardware (GPIO, PWM, UART, I2C, SPI), the user (here: \textit{www-data}) must be given appropriate permissions. We can  edit user permissions by running the \lstinline{sudo visudo} command. In order to give the user the permission to execute a given program, add the following line:\\
 \lstinline{www-data ALL = (root) NOPASSWD: /path/to/my/program_or_script}.\\ Separate file names with a comma. 

\bigbreak

\begin{tcolorbox}[colback=purple,coltext=black]
\textbf{NOTE!} At the stage of development and testing of the server application, a convenient option is to \textbf{temporary} give the server root privileges: \lstinline{www-data ALL = (ALL) NOPASSWD: ALL}. However, this is an absolutely unacceptable situation in the final product, for safety reasons. Do not use this configuration while accessing the Internet!
\end{tcolorbox}

\bigbreak

On the listing. \ref{lst:php1} an example PHP script using system shell scripts to operate GPIO is shown. Fig \ref{fig:ff}. shows script execution result in Firefox for various arguments: \textit{ON} and \textit{OFF}. If we provide the appropriate GPIO configuration in advance and connect the LED diode according to the diagram (Fig. \ref{fig:sch}), we obtain the ability to control the LED state from the level of every device equipped with a web browser connected to the same \textbf{local} network.

\lstinputlisting[style=MyPHPStyle, 
caption={ An example of a PHP script controlling a LED (via GPIO) with a system shell script. }, 
label={lst:php1}]{lst/server_examples/led.php}

\begin{figure}[H]
\includecgraphics[width=9.5cm]{ff.png}
\caption{ Server response - script controlling LED state (via GPIO). }
\label{fig:ff}
\end{figure}

%% TODO
%% CGI + Python

%% TODO
%% OpenSSL 

%% -- END Lighhtpd --------------------------------------------------------------------------
\section{Scenario for the class}

\subsection{Teaching resources}
\begin{tabular}{ l  p{13cm} }
 Hardware   & \tabitem computer, \\
            & \tabitem Raspbery Pi single-board computer set, \\
            & \tabitem miroSD card, \\
            & \tabitem microUSB 5V power supply, \\
            & \tabitem RJ45 cable, \\
            & \tabitem \textit{set of electronic components,} \\
            & \tabitem \textit{multimeter}, \\
            & \tabitem \textit{oscilloscope}, \\
            & \tabitem \textit{WiFi router.} \\
 Software & \tabitem SSH client (Bitwise SSH Client),    \\
 			& \tabitem application for writing binary disk images to external media (eg Win32DiskMaker), \\
 			& \tabitem text editor (Notepad++), \\
 			& \tabitem VirtualBox appliaction and virtual machine with Raspbian system.
\end{tabular}

\subsection{Tasks} \label{exercises}

\subsubsection{Configuration of network interfaces}
\begin{enumerate}

\item \textbf{Physical device.}
\begin{enumerate} 
\item Connection with RPi via SSH.
%% exercise 01A ------------------------------------------------------------------ 
	\begin{enumerate}
	\item Download the disk image with configured Raspbian system, provided by the instructor.
	\item Unpack the archive and save the image to a microSD card (min. 16 GB) using an appropriate tool (e.g. Win32DiskMaker)
	\item Connect RPi to the local network (via router or directly to the PC) using Ethernet. Remember that the default \textbf{static} IP address for the transferred system is \textbf{192.168.1.15}. Configure second devices (PC) accordingly.
	\item Connect RPi power supply and wait few seconds.
	\item Connect to RPi using an SSH client (e.g. Bitwsie SSH Client). Use default port 22. Username: \textbf{pi}, password:  \textbf{raspberry} (default values).
	\item After starting the terminal, check the configuration of the network interfaces.
	\item Does RPi have internet access? Check with \textbf{ping} command.
	\end{enumerate}

\item Configuration of \textbf{wlan0}.
%% exercise 02A ------------------------------------------------------------------ 
	\begin{enumerate}
	\item Configure interface \textbf{wlan0} (WiFi) - add information about your wireless network (SSID and password).
	\item Reset network interfaces with the \lstinline{sudo service networking restart} command or restart RPi.
	\item Check interface configuration after reboot. Does RPi have internet access? Check with the \textbf{ping} command .
	\item Set static IP for \textbf{wlan0}. Restart the interface or device.
	\item Check interface configuration after reboot. Does RPi have internet access? Check with the \textbf{ping} command.
	\item Connect to RPi via SSH using the static address \textbf{wlan0}.
	\end{enumerate}
\end{enumerate}
	
\item \textbf{Virtual device.}
\begin{enumerate} 
\item Connection with RPi via SSH.
%% exercise 01B ------------------------------------------------------------------ 
	\begin{enumerate}
	\item Download a virtual device with the configured Raspbian system, provided by the instructor.
	\item Import the device into VirtualBox and start the system.
	\item Check the configuration of network interfaces directly in the virtual device terminal.
	\item Connect to RPi using an SSH client (e.g. Bitwsie SSH Client). Use the IP address of the \textbf{eth1} interface. Use default port 22. Username: \textbf{pi}, password: \textbf{raspberry} (default values).
	\item After starting the terminal, check the configuration of the network interfaces.
	\item Does RPi have internet access? Check with \textbf{ping} command.
	\end{enumerate}

\item Configuration of \textbf{eth1}.
%% exercise 02B ------------------------------------------------------------------ 
	\begin{enumerate}
	\item Set static IP for \textbf{eth1}. Restart the interface or device.
	\item Check interface configuration after reboot. Does RPi have internet access? Check with the \textbf{ping} command.
	\end{enumerate}
\end{enumerate}

\end{enumerate}

\subsubsection{Command line applications (CLIs)}
\begin{enumerate}
%% exercise 02 ------------------------------------------------------------------ 
\item Create three text files: \lstinline{temperature.dat humidity.dat pressure.dat}.
\item Write the following content accordingly: \lstinline {20.5C 80% 1023.0hPa}.
\item Write the three command line application (CLIs): \textit{bash} system shell script, program in Python3, and program in C++. Create an automatic compilation script for a C++ program. All three applications should perform exactly the same task and have \textbf{identical interfaces}.
\item The application has the following (optional) arguments: \lstinline{-t -h -p}. If a given flag is present, it means that the application should read the appropriate text file and display its content (in the separate lines). So if there are no arguments, the program does not perform any action, if there are all three displays three lines of text.
\item Extend the application with the option of choosing a temperature unit - after  \lstinline{-t} option should be given the unit: \lstinline{C} (Celsius degrees) or \lstinline{F} (Fahrenheit degrees). Based on this selection, the program should display the original or scaled value saved in the file.
\end{enumerate}

\subsubsection{Peripheral devices}
\begin{enumerate}
%% exercise 03 ------------------------------------------------------------------ 
\item Write a simple application to operate GPIO. 
	\begin{enumerate}
	\item Build the electronic circuit shown in Fig. \ref{fig:sch}.
	\item Test the example scripts and programs.
	\item Write your own command line application: use the button to change the period of the LED status change.
	\item Add support for input arguments that allow you to configure the application: maximum and minimum period of LED toggle; period change step.
	\end{enumerate}
	
\item Write a simple application to operate PWM. 
	\begin{enumerate}
	\item Build the electronic circuit shown in Fig. \ref{fig:sch}.
	\item Test the example scripts and programs.
	\item Write your own command line application: expand the presented interfaces with the option to configure not only duty but also frequency.
	\end{enumerate}
	
%\item Write a simple application to operate UART. \textit{TODO} 

%\item Write a simple application to operatei I2C. \textit{TODO} 

%\item Write a simple application to operate SPI.\textit{TODO} 

\end{enumerate}

\subsubsection{Server applications}
\begin{enumerate}
%% exercise 04 ------------------------------------------------------------------ 
\item Configure a Lighttpd server - change the work path to \lstinline{/home/pi/server_examples}. Grant the appropriate permissions to the \lstinline{www-data} user.
\item Add the script \lstinline{test.php} with the content \lstinline{<?php echo ,,Hello RPi''; ?>}. Check in your browser - using the RPi IP address - whether the server is available and whether the script is executing correctly.
\item Create a script using command line applications. For a physical device, write a PHP script that allows you to change the period of LED blinking using the GET method. For a virtual device, write a PHP script that will allow you to read and write to the file \lstinline{led_blink_period.dat}. The scripts should return the status (error or success), the argument passed, and contents of the file in the form of a JSON structure.
	
\end{enumerate}

\bibliographystyle{IEEEtran}
\bibliography{../TemplateLatex/AM_REF} 

\end{document}
