#!/usr/bin/python3
#**
#******************************************************************************
#* @file    /pwm_examples/python/pwm_example.py
#* @author  Adrian Wojcik
#* @version V1.0
#* @date    15-Mar-2020
#* @brief   Raspberry Pi PWM control: Python 3 with RPi.GPIO lib
#******************************************************************************

import time
import sys
import select
import numpy

try:
	import RPi.GPIO as GPIO
except RuntimeError:
	print("Error importing RPi.GPIO! This is probably because you need superuser privileges. You can achieve this by using 'sudo' to run your script")

timeout = 1	
i = ''	
duty = 0 # [%]
freq = 50 # [Hz]
	
# Pin Definitons:
pwmPin = 12    #< LED: Physical pin 16, BCM GPIO23	
	
GPIO.setmode(GPIO.BOARD)
GPIO.setup(pwmPin, GPIO.OUT)

p = GPIO.PWM(pwmPin, freq)  
p.start(duty)

print("Press ENTER to exit.")

while not i:
	# Waiting for I/O completion
	i, o, e = select.select( [sys.stdin], [], [], timeout )

	if (i):
		sys.stdin.readline();
		p.stop()
		GPIO.cleanup() # cleanup all GPIO
		exit()
		
	for d in numpy.arange(0, 100, 0.5):
		p.ChangeDutyCycle(d)
		time.sleep(0.01)
	for d in numpy.arange(100, 0, -0.5):
		p.ChangeDutyCycle(d)
		time.sleep(0.01)
		