const jsObjExample = {
    temperature: { value: 20.3,   unit: "C"   },
    pressure:    { value: 1023.0, unit: "hPa" },
    humidity:    { value: 63,     unit: "%"   }
};

const jsonTextExample ='{"temperatur":{"value":20.3,"unit":"C"},"pressure":{"value":1023.0,"unit":"hPa"},"humidity":{"value":63,"unit":"%"}}';

var jsObj = JSON.parse(jsonTextExample);
var jsonText = JSON.stringify(jsObjExample);