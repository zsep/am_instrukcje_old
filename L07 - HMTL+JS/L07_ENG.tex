\documentclass[11pt, a4paper]{article}

\usepackage[]{../TemplateLatex/am} % [pl] or []

%%% Definitions %%%
\university{Poznań University of Technology}
\institute{Institute of Robotics and Machine Intelligence}
\department{Division of Control and Industrial Electronics}
\lab{Mobile and embedded applications for Internet of Things}
\author{Dominik Łuczak, Ph.D.; Adrian Wójcik, M.Sc.}
\email{Dominik.Luczak@put.poznan.pl \\ Adrian.Wojcik@put.poznan.pl}

\maintitle{ Web application \\ HTML and JavaScript}

\comment{Teaching materials for laboratory}
%%
%%

\begin{document}

%% First page %%
\mainpage

%%
%%

\section{Goal}

\subsection*{Knowledge}
The aim of the course is to familiarize yourself with: 
\begin{itemize}[label={\textbullet}]
\item the role of HTML in web applications,
\item syntax of HTML and basic HTML tags,
\item HTML document structure,
\item the role of the Document Object Model in web applications,
\item the role of JavaScript in web applications,
\item basics of JavaScript language syntax.
\end{itemize}

\subsection*{Skills}
The aim of the course is to acquire skills in:
\begin{itemize}[label={\textbullet}]
\item creating HTML documents,
\item creating scripts in JavaScript language, 
\item communicating JS scripts with HTML documents.
\end{itemize}

\subsection*{Social competences}
The aim of the course is to develop proper attitudes:
\begin{itemize}[label={\textbullet}]
\item proper management of web application code base,
\item strengthening understanding and awareness of the importance of non-technical aspects and effects of the engineer's activities, and the related responsibility for the decisions taken,
\item choosing the right technology and programming tools for the given problem,
\item testing developed IT system.
\end{itemize}

\section{Final Task}
% fixed component
\input{../TemplateLatex/POLECENIA_KONCOWE_ENG.tex}

\section{Prepare to course}

\subsection{Know the safety rules}
% fixed component
\input{../TemplateLatex/BHP_ENG.tex}

\subsection{Introduction to HTML}
%% -- BEGIN HTML introduction ----------------------------------------------------------------
HTML (\textit{HyperText Markup Language}) is the most basic component of the World Wide Web. HTML defines \textbf{structure} of web content. (\textit{Hypertext}) refers to links that connect web pages to each other, both within single domain page and between different domains. The HTML element is distinguished from plain text in the document by \textit{markup} or more commonly 'tags', which consist of the name of the element surrounded by ,,<'' and ,,>''. The element name inside the tag is \underline{not} case-sensitive. This means that it can be in uppercase, lowercase or mixed letters. For example, the \lstinline{<title>} tag can be also written as \lstinline{<Title>}, \lstinline{<TITLE>}, or in other ways \cite{htmlmozilla}.\\

Currently (since May 2019) the WHATWG organization (\textit{Web Hypertext Application Technology Working Group}) is the publisher of HTML and DOM standrads. The language standard can be found on the WHATWG page \cite{html} . The current version of HTML language is HTML5 \cite{html5}. A list of tags with descriptions and examples is available on the \href{https://devdocs.io}{devdocs.io} portal \cite{htmldoc}.\\

Separate technologies apart from HTML used to create web applications are: \textbf{CSS} used to describe the appearance/presentation of the website and \textbf{JavaScript} used to describe the functionality/behavior of the application \cite{htmlmozilla}.
%% -- END HTML introduction ------------------------------------------------------------------

\subsection{Document object model}
%% -- BEGIN DOM -------------------------------------------------------------------------
DOM (\textit{Document Object Model}) is an API that represents HTML documents that allows to programmatically communicate with them. DOM is a model of a document presented in a browser represented as a tree, in which each \textit{node} represents a part of the document (see: fig. \ref{fig:dom}).

\begin{figure}[H]
\includecgraphics[width=10.0cm]{dom.png}
\caption{An example of the DOM hierarchy in an HTML document \cite{domwiki}. }
\label{fig:dom}
\end{figure}

The introduction of a structural representation of the document, allows to modify its content and visual presentation. DOM is one of the most commonly used APIs in the WWW because it allows access and complex interaction with  \textit{nodes} of the document. These nodes can be created, moved and changed. Also, event listener can be add to nodes. The DOM therefore combines web pages with JavaScript (or other programming languages). The DOM standard can be found at WHATWG  website \cite{dom}.
%% -- END DOM ---------------------------------------------------------------------------

\subsection{Introduction to JavaScript}
%% -- BEGIN JS introduction ----------------------------------------------------------------
JavaScript (in short: \textit{JS}) is a scripting programming language - interpreted or compiled using the JIT (just-in-time) method . A distinctive feature of JS is that functions are ,,first-class citizens'', i.e. objects that can be stored as references and passed as any other objects. JavaScript is a multi-paradigm language: prototype-based, dynamic syntax, object-oriented, imperative and declarative (functional programming) \cite{jsmozilla}.\\

The standard for JavaScript is ECMAScript \cite{js}. In June 2015, ECMA International published the sixth major version of ECMAScript. Since 2012, all modern browsers completely support ECMAScript 5.1. Older browsers support at least ECMAScript 3. Currently, ECMAScript standards are issued on an annual basis. \\

Do not confuse JavaScript with the Java programming language. Both ,,Java'' and ,,JavaScript'' are trademarks of Oracle. However, both of these programming languages have very different syntax, semantics, and uses. This name is primarily the result of marketing efforts.\\

JavaScript is best known as a scripting language for websites, but it is also used by many non-browser environments such as Node.js, Apache CouchDB and Adobe Acrobat.

\subsubsection{JavaScript in HTML document}
%%>>> -- BEGIN JS in HTML ------------------------------------------------------------------
On the listing \ref{lst:html1}. an example of an HTML document with a script written in JavaScript language is presented. Script allows handling of the event in the form of pressing a button. Both HTML and JS are in single \textbf{.htm} file.

\lstinputlisting[style=MyHTMLStyle, 
caption={ An example of an HTML document with JavaScript in single file. },
label={lst:html1}]{lst/button_example_v1.htm}
\vspace{0.5cm}
When creating web applications, however, it is good practice to separate the description of \textit{document structure} (HTML) from \textit{software functionality} (JavaScript). On the listings \ref{lst:html2}. and \ref{lst:js}. are presented the \textbf{.htm} file (containing information about used \textbf{.js} file) and the \textbf{.js} file, respectively. Of course, both presented web applications  work in an identical way.
\newpage
\lstinputlisting[style=MyHTMLStyle, 
caption={ An example of an HTML document with JavaScript in a separate file. },
label={lst:html2}]{lst/button_example_v2.htm}

\lstinputlisting[style=MyJavaScriptStyle, 
caption={An example of JavaScript in a separate \textbf{.js} file. },
label={lst:js}]{lst/button_example_v2.js}
\vspace{0.5cm}
Using the \lstinline{<script>} tag, external JS libraries that are available both locally and as an online resource, can be add to document.

%%>>> -- END JS in HTML --------------------------------------------------------------------
\subsubsection{JSON in JavaScript}
%%>>> -- BEGIN JSON in JS ------------------------------------------------------------------
The JSON (\textit{JavaScript Object Notation}) data exchange format is derived directly from JavaScript - JSON objects are \textbf{textual} representation of objects in JavaScript. The function enabling parsing, i.e. obtaining a JS object from text in JSON format (\lstinline{JSON.parse()}) and encoding, i.e. obtaining text in JSON format from a JS object (\lstinline {JSON.stringify()}) are both part of the JS language and do not require use of additional libraries. On listing \ref{lst:json}. an example use case of these functions is shown.

\lstinputlisting[style=MyJavaScriptStyle, 
caption={ An example use case of functions \lstinline{JSON.parse()} and \lstinline{JSON.stringify()}. },
label={lst:json}]{lst/json_example.js}
\vspace{0.5cm}
%%>>> -- END JSON in JS -------------------------------------------------------------------- 
\subsubsection{JavaScript code debugging in web browser}
Most modern browsers contain \textit {web console}, that allows to view variables and execute JavaScript code in the current \textit {context}. There are also tools available that allows full debugging of the web application directly in the browser - e.g. Firefox or Chrome. Fig. \ref{fig: debug} shows an example of debugging a simple button application.

\begin{figure}[H]
\includecgraphics[width=15.0cm]{js_debug.png}
\caption{An example of debugging a JS script in the Chrome browser - stopping script execution using \textit{breakpoint} in line 8. after pressing ,,Click me'' button.}
\label{fig:debug}
\end{figure}

%% -- END JS introduction ------------------------------------------------------------------
\section{Scenario for the class}

\subsection{Teaching resources}
\begin{tabular}{ l  p{13cm} }
 Hardware  & \tabitem computer, \\
 Software  & \tabitem web browser (eg. Firefox, Chrome), \\
 		   & \tabitem text editor (eg. Notepad++), \\
 		   & \tabitem web server (eg. XAMPP, Lighttpd) (optional),
\end{tabular}

\subsection{Tasks} \label{exercises}

\begin{enumerate}

\item Create a simple HTML document with a single button.
	\begin{enumerate}
    \item Using a text editor, create HTML document (*.htm) and JavaScript script (*.js).
    \item Place the header section and body section in the HTML document. Define documents title, add script information, and put a single ,,CLICK ME'' button in page body. Assign the name of your JavaScript function to \lstinline {onClick} attribute.
    \item In the JavaScript script, define the function to handle button clicking. The function should change the content of the button to ,,HELLO WORLD'' and increment some global variable. 
    \item Open the HTML document using a browser and check if the defined functionality works correctly.
    \item Open the browser's web console and check that the global variable defined in the script changes the value correctly each time the button is clicked.
    \item Alternatively, place the document and script on the local server, then open the document by entering the appropriate URL in your browser.
	\end{enumerate}

\item Create a simple web application: unit converter.
	\begin{enumerate}
	\item Create an HTML document containing elements that allow the user to enter a numerical value (e.g. rotational speed), select the input and output unit (min. four diffrent units, e.g. rpm, rps, rad/s, rad/min, deg/s, deg/min), confirmation of input data and presentation of the result. Use the \lstinline{input}, \lstinline{select} and \lstinline{output} tags.
	\item Create a JavaScript script for unit conversions. Try to implement this functionality \underline{without} using conditional statements.
	\end{enumerate}
	
\item Modify a simple web application using JSON as input and output.
	\begin{enumerate}
	\item Based on the solution to the previous task, create a web application that converts units (e.g., rotational speed) based on the structure provided by the user in JSON format. The structure must contain information about the input value, input unit and output unit. Include an example structure as the starting value for the text input element.
	\item The application output should also be presented in JSON format, containing information about the output value and output unit.
	\item Validate the input data, inform the user about any errors (e.g. invalid JSON format, incorrect input value, incorrect / unavailable input unit, incorrect / unavailable output unit).
	\end{enumerate}

\item Create a web application containing a function graph using an external JavaScript library.
	\begin{enumerate}
	\item Familiarize yourself with the documentation of the Chart.js library available on the website: \href{https://www.chartjs.org/}{chartjs.org}.
	\item Create an HTML document and add the Chart.js library to it with: \\
	\lstinline{<script type="text/javascript"  } \\
	\lstinline{     src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0">} \\
	\lstinline{<\script>  } \\
	Add your own script. The order of placing scripts in the document \underline{is} important in the context of visibility of the variables declared in the scripts.
	\item Place a (\textit{line chart}) in the document - based on examples in the Chart.js library documentation. Add elements to allow user input. The document should allow input of information about the sample time, amplitude, phase and period of the harmonic signal.
	\item In your script, implement the functionality that allows to generate data for the chart based on the harmonic signal parameters provided by the user and update the data and description of the x axis of the chart.
	\end{enumerate}
	
\item Create basic web application consisting of multiple HTML documents.
	\begin{enumerate}
	\item Create an HTML document with three elements: a heading (title), 4 hyperlinks, and a frame.
	\item Hyperlinks should be links to files with solutions to previous tasks. After clicking the hyperlink, the frame should be filled with the content of the appropriate document.
	\item Answer the question: Is JavaScript necessary to achieve this functionality?
	\end{enumerate}
\end{enumerate}

\bibliographystyle{IEEEtran}
\bibliography{../TemplateLatex/AM_REF} 

\end{document}
