\documentclass[11pt, a4paper]{article}

\usepackage[]{../TemplateLatex/am} % [pl] or []

%%% Definitions %%%
\university{Poznań University of Technology}
\institute{Institute of Robotics and Machine Intelligence}
\department{Division of Control and Industrial Electronics}
\lab{Mobile and embedded applications for Internet of Things}
\author{Dominik Łuczak, Ph.D.; Adrian Wójcik, M.Sc.}
\email{Dominik.Luczak@put.poznan.pl \\ Adrian.Wojcik@put.poznan.pl}


\maintitle{ Web application \\ jQuery / CSS }

\comment{Teaching materials for laboratory}
%%
%%

\begin{document}

%% First page %%
\mainpage

%%
%%

\section{Goal}

\subsection*{Knowledge}
The aim of the course is to familiarize yourself with: 
\begin{itemize}[label={\textbullet}]
\item role of JavaScript libraries in web applications on the example of jQuery,
\item syntax and available functions of jQuery library,
\item AJAX technique in the context of jQuery,
\item basics of CSS syntax,
\item using CSS libraries on the example of Bootstrap.
\end{itemize}

\subsection*{Skills}
The aim of the course is to acquire skills in:
\begin{itemize}[label={\textbullet}]
\item creating web applications using the AJAX technique,
\item creating web applications using the jQuery library,
\item defining the style of the web application user interface using CSS.
\end{itemize}

\subsection*{Social competences}
The aim of the course is to develop proper attitudes: 
\begin{itemize}[label={\textbullet}]
\item proper management of web application code base,
\item strengthening understanding and awareness of the importance of non-technical aspects and effects of the engineer's activities, and the related responsibility for the decisions taken,
\item choosing the right technology and programming tools for the given problem,
\item testing developed IT system.
\end{itemize}

\section{Final Task}
% fixed component
\input{../TemplateLatex/POLECENIA_KONCOWE_ENG.tex}

\section{Prepare to course}

\subsection{Know the safety rules}
% fixed component
\input{../TemplateLatex/BHP_ENG.tex}

\subsection{Introduction to jQuery}
%% -- BEGIN jQuery introduction ----------------------------------------------------------------
jQuery is a JavaScript library that primarily simplifies the use of the HTML Document Object  Model (DOM). Important jQuery functionalities are also: event handling, animation and style support, and a set of methods implementing the AJAX technique \cite{jq} . It is free \textit{open source} software using the MIT License (X11 License) \cite{jqlicense}. The jQuery library is used by almost $\sfrac{3}{4}$ of websites (as of April 2020) \cite{jqstat}. \\

jQuery has a simple syntax in which three basic elements can be distinguished:
\begin{itemize}[label={\textbullet}]
\item the character '\$' (dollar sign) denotes the alias of the jQuery library (it can be replaced with the word \lstinline{jQuery}, however this notation is not typically used in practice),
\item a selector in the form \lstinline{("selector")} where the attribute \textit{selector} is the name of the HTML element, the ID of the HTML element (with the symbol '\#' at the beginning) or the class of the HTML element (with the symbol '.' at the beginning).
\item action (function) name after the dot.
\end{itemize}

\begin{figure}[H]
\includecgraphics[width=10.0cm]{jquerysyntax.png}
\caption{The structure of the jQuery syntax.}
\label{fig:jqsyntax}
\end{figure}

Fig. \ref{fig:jqsyntax} shows the structure of the jQuery syntax. Detailed API documentation with use case  examples is available on the project website \cite{jqdoc}.

%% -- END jQuery introduction ------------------------------------------------------------------

\subsection{Asynchronous JavaScript and XML}
%% -- BEGIN AJAX -------------------------------------------------------------------------
AJAX (Asynchronous JavaScript and XML) is \textit{technique} (not stand-alone \textit{technology}) for creating web applications using HTML documents represented by DOM, JavaScript script language and CSS style sheets. Thanks to the properly implemented AJAX model, web applications are able to make quick, incremental updates in the user interface without the need to reload the entire web page. As a result, the application seems faster and gives a better user experience \cite{ajax}. AJAX technique, despite the name, is increasingly giving up the XML data format in favor of the JSON data format. \\

A typical scenario for using the AJAX technique is as follows: \cite{ajaxw3}:
\begin{enumerate}
\item Event takes place on the website (e.g. a button has been pressed).
\item XMLHttpRequest object is created by JavaScript.
\item XMLHttpRequest object sends a request to a web server (e.g. an IoT server).
\item Server application is processing the request.
\item Server sends the response back to the website.
\item Response is read and decoded by JavaScript.
\item Proper action (usually interacting with the HTML document) is performed by JavaScript.
\end{enumerate}

\begin{figure}[H]
\includecgraphics[width=10.0cm]{ajaxmodel.png}
\caption{Web application model: classic vs. AJAX \cite{ajaxjacek}. }
\label{fig:ajaxmodel}
\end{figure}

Fig. \ref{fig:ajaxmodel} presents a comparison of the classic web application model with the AJAX model. This technique is often used in cooperation with the REST architecture, which in turn allows for obtaining clearly defined and legible client-server communication.

%% -- END AJAX ---------------------------------------------------------------------------

\subsection{Introduction to CSS}
%% -- BEGIN CSS introduction ----------------------------------------------------------------
Cascading Style Sheets (CSS) is a language that describes \textit{style} of an HTML document, i.e. how HTML elements are displayed. The name ,,cascading'' comes from the priority scheme specified in CSS to determine which style rule applies if more than one rule matches a particular element. Separation of content (HTML) and formatting (CSS) allows document with the same markers structure to be presented in different styles for different rendering methods, including alternative formatting if e.g. the content is accessed on a mobile device \cite{css}\cite{cssdev}. CSS has different \textit{levels} and \textit{profiles}. Each level of CSS builds on an earlier level, usually adding new features; they are currently marked as CSS 1, CSS~2, CSS 3 and CSS 4. \\

Styles defined using CSS can be placed directly in an HTML document using the \lstinline{<style>} tags or by using an external file (also the web repository) using the \lstinline{<link>} tag (Listing \ref{lst:css1}).

\lstinputlisting[style=MyHTMLStyle, 
caption={ An example of the header of an HTML document with CSS. },
label={lst:css1}]{lst/css.htm}

\newpage

Fig. \ref{fig:csssyntax} shows the structure of CSS syntax. As with the jQuery library selectors in CSS, we can define the style of an HTML element by its name, ID (with the symbol '\#' at the beginning) or class (with the symbol '.' At the beginning). On the W3C consortium website can be found language documentation along with a list of supported atributes of HTML elements \cite{cssdoc}.

\begin{figure}[H]
\includecgraphics[width=15.0cm]{csssyntax.png}
\caption{CSS syntax structure.}
\label{fig:csssyntax}
\end{figure}

%% -- END CSS introduction ------------------------------------------------------------------

\subsection{CSS library example: Bootstrap}
%% -- BEGIN Bootstrap introduction ----------------------------------------------------------------
One of the reasons for the huge popularity of CSS technology is the availability of many libraries facilitating creation of aesthetic and ergonomic interfaces of web applications with little effort from the developer. An example of a popular, free and open CSS library is Bootstrap \cite{bootstrap}. This library contains CSS-based design templates and (optional) JavaScript for typography, forms, buttons, navigation and other user interface elements. Fig. \ref{fig:cssexample} presents an example of a simple \ textit{data grabber} application developed using one of Bootstrap template.

\begin{figure}[H]
\includecgraphics[width=15.0cm]{cssexample.png}
\caption{An example of a web application with Bootstrap.}
\label{fig:cssexample}
\end{figure}

%% -- END Bootstrap introduction ------------------------------------------------------------------

\section{Scenario for the class}

\subsection{Teaching resources}
\begin{tabular}{ l  p{13cm} }
 Hardware  & \tabitem computer, \\
 Software  & \tabitem web browser (eg. Firefox, Chrome), \\
 		   & \tabitem text editor (eg. Notepad++), \\
 		   & \tabitem web server (eg. XAMPP, Lighttpd) (optional),
\end{tabular}

\subsection{Tasks} \label{exercises}

Create a web application for the Sense HAT board. Use the AJAX technique. Use CSS to format the style of the document. The application should contain the following modules:
\begin{enumerate}
\item Presentation of measurement data. The module should allow the presentation of temperature / pressure / humidity measurements OR RPY angles using a  timeseries chart. User interface must allow simultaneous view of 3 variables.
\item LED display control. The module should allow the control of individual LEDs (minimum) and group LED control (e.g. as text, image, lines, columns, etc.).
\item Configuration of basic application settings, such as server IP address, port used, server API version (convenient for the developer), sampling period, maximum number of stored samples, etc. Configuration information should be saved on \textbf{server} as a text file in JSON format.
\end{enumerate} 

\bibliographystyle{IEEEtranPL}
\bibliography{../TemplateLatex/AM_REF}

\end{document}
